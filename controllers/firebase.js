var request = require('request');

// Firebase server api key
var FIREBASE_API_KEY = 'AAAARBNWdE8:APA91bEoh8JEmF9jm3nh-y3qyPIO4_NNxq2Y2iDxXu2p9rsW9RLsc8stFzpfnV0kOKkHdtI8bQHCp-s272DJNL4hQgROt4xksRDMGc5AYgU8nuKObrnZb7h2-xbu9IQED-9Svy1lU-Qw';

// tokennya is an auth_key from table couriers provided by Firebase
var sendNotificationToUser = function(token, title, content) {
  request({
    url: 'https://fcm.googleapis.com/fcm/send',
    method: 'POST',
    headers: {
      'Content-Type' :' application/json',
      'Authorization': 'key='+FIREBASE_API_KEY
    },
    body: JSON.stringify({
      to : token,
      data : {
      	text : content,
      	title : title
      }
    })
  }, function(error, response, body) {
    
  });
}

module.exports = {
	sendNotificationToUser : sendNotificationToUser
}