var express = require('express');
var router = express.Router();
var userControllers = require('../controllers/user.js');

router.use(function(req, res, next) {
  if(req.session.email) {
    next();
  }
  else {
    res.redirect('/login');
  }
});

/* GET list Users page */
router.get('/view-users', function (req, res, next) {
	userControllers.getUsers(req, res).then(function (users) {
		res.render('view-users', {
      users : users,
      title : 'View Users',
      adminName: req.session.fullname,
      successCreateUser: req.flash('successCreateUser'),
      successDeleteUser: req.flash('successDeleteUser')
    });
	}).catch(function (err) {
		res.send(err.message);
	})
});

router.post('/view-userDetails', function (req, res, next) {
  req.params.id = req.body.id;
  userControllers.getUserById(req, res).then(function (user) {
    res.send(user[0]);
  }).catch(function (err) {
    res.send(err.message);
  })
});

router.get('/create-user', function(req, res, next) {
  userControllers.getRoles(req,res).then(function(result) {
    res.render('create-user', {
      roles: result,
      adminName: req.session.fullname,
      fullname: req.flash('fullname'),
      email: req.flash('email'),
      phone: req.flash('phone'),
      error: req.flash('errors'),
      title: 'Create User',
      submitvalue: 'Create',
      actionForm: '/user/create-user'
    });
  }).catch(function(err) {
    res.send(err.message);
  });
});

router.post('/create-user', function(req, res, next){
  req.check('fullname', 'Karakter Name kurang dari 1 atau lebih dari 32').isLength({min:1, max: 32});
  req.check('password', 'password terdiri dari 5 sampai 18 karakter').isLength({min:5, max: 18});
  req.check('email', 'Format email salah').isEmail();
  req.check('phone', 'Phone hanya boleh angka').isNumeric();
  req.check('roles','roles tidak boleh kosong').notEmpty();

  var errors = req.validationErrors();

  if (errors){
    req.flash('errors',errors);
    req.flash('fullname',req.body.fullname);
    req.flash('email',req.body.email);
    req.flash('phone',req.body.phone);
    req.flash('roles',req.body.roles);
    res.redirect('/user/create-user/');
  }
  else{
    req.body.avatar = '/img/avatar/default.png';
    userControllers.createUser(req, res).then(function(InsertionStatus){
      req.flash('successCreateUser','User has been created!');
      res.redirect('/user/view-users');
    }).catch(function(err){
      res.send(err.message);
    });
  }
});

router.get('/update/', function (req, res, next) {
  res.redirect('/user/create-user/');
});

router.post('/update/', function (req, res, next) {
  res.redirect('/user/create-user/');
});

router.get('/update/:id', function (req, res, next) {
  userControllers.getUserById(req, res, next).then(function (result) {
    userControllers.getRoles(req, res).then(function (allRoles) {
      if (typeof result !== 'undefined' && result.length > 0) {
        var roleName = "";

        for (var i = 0; i < allRoles.length; i++) {
          if(allRoles[i].id == result[0].roles_id) {
            roleName = allRoles[i].role_name;
          }
        }

        res.render('create-user', {
          fullname: result[0].fullname,
          email: result[0].email,
          phone: result[0].phone,
          roleName: roleName,
          roles: allRoles,
          title: 'Update User',
          adminName: req.session.fullname,
          submitvalue: 'Update',
          actionForm: '/user/update/' + req.params.id,
          success: req.session.success,
          error: req.flash('errors'),
          updateForm: 'updateUser'
        });
        req.session.errors = null;
        req.session.success = null;
      }
      else{
        res.redirect('/user/create-user/');
      }
    });
  }).catch(function (err) {
    res.send(err.message);
  });
});

router.post('/update/:id', function (req, res, next) {
  userControllers.getUserById(req, res, next).then(function (oldUser) {
    req.check('fullname', 'Karakter Name kurang dari 1 atau lebih dari 32').isLength({min:1, max: 32});
    req.check('email', 'Format email salah').isEmail();
    /**  req.check('password', 'password invalid').equals(oldUser[0].password);*/
    req.check('phone', 'Phone hanya boleh angka').isNumeric();
    req.check('roles','roles tidak boleh kosong').notEmpty();
    var errors = req.validationErrors();

    if (errors){
      req.flash('errors',errors);
      req.flash('fullname',req.body.fullname);
      req.flash('email',req.body.email);
      req.flash('phone',req.body.phone);
      req.flash('roles',req.body.roles);
      res.redirect('/user/update/' + req.params.id);
    }
    else{

      req.body.avatar = '/img/avatar/default.png';
      if(req.body.newPassword != "" || req.body.confirmPassword != "") {
        req.check('newPassword', 'password terdiri dari 5 sampai 18 karakter').isLength({min:5, max: 18});
        req.check('confirmPassword', 'password terdiri dari 5 sampai 18 karakter').isLength({min:5, max: 18});
        req.check('confirmPassword', 'password tidak sama').equals(req.body.newPassword);
        var errors = req.validationErrors();
        if(errors) {
          req.flash('errors',errors);
          req.flash('fullname',req.body.fullname);
          req.flash('email',req.body.email);
          req.flash('phone',req.body.phone);
          req.flash('roles',req.body.roles);
          res.redirect('/user/update/' + req.params.id);
        } else {
          req.body.password = req.body.confirmPassword;
        }
      }
      req.body.password = req.body.confirmPassword;
      userControllers.createUser(req, res).then(function(InsertionStatus){
        req.flash('successCreateUser','User has been updated!');
        res.redirect('/user/view-users');
      }).catch(function(err){
        res.send(err.message);
      });
    }
  });
});

router.get('/delete/:id', function (req, res, next) {
  userControllers.deleteUser(req, res).then(function(Sequelizeresponse){
    if(Sequelizeresponse > 0){
      Sequelizeresponse = "Number of deleted row is "+Sequelizeresponse;
    }
    req.flash('successDeleteUser', 'User has been deleted!')
    res.redirect('/user/view-users');
  }).catch(function(err){
    console.log(err.message);
    if (err.message >= 100 && err.message < 600)
      res.sendStatus(err.message);
    else
    res.sendStatus(500);
  });
});
module.exports = router;
