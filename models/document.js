var Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  var Sequelize = require('sequelize');

  var Document = sequelize.define("document", {
  	id: {type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true},
    name: {type: DataTypes.TEXT, allowNull: false},
    task_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'task',
        key: 'id',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
      }
    },
    quantity: {type: DataTypes.INTEGER, allowNull: false},
    description: {type: DataTypes.TEXT, allowNull: false}
  }, {
    associate: function(models) {
      Document.belongsTo(models.task);
    }
  });

  return Document;
};
