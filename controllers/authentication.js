var model = require('../models');
var Sequelize = require('sequelize');
var Promise = require('bluebird');

var login = function(req, res, next) {
	var user = { email: req.body.email };

	return validateUser(req.body);
}

var logout = function(req, res, next) {
	return true;	
}

/* for validate user */
function validateUser(user) {
	var user = model.user.findOne({
		where: { email: user.email },
		raw: true
	});

	return user;
}

module.exports = {
	login : login,
	logout : logout
};