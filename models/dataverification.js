var Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  var Sequelize = require('sequelize');

  var DataVerification = sequelize.define("data_verification", {
  	order_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'task',
        key: 'id',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
      }
    },
    image: {type: DataTypes.TEXT, allowNull: false},
    signature: {type: DataTypes.TEXT, allowNull: false},
    timestamp: {type: DataTypes.DATE, allowNull: false},
    note: {type: DataTypes.TEXT}
  });
  return DataVerification;
};
