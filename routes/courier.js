var express = require('express');
var router = express.Router();
var courier = require('../controllers/courier.js');

router.use(function(req, res, next) {
  if(req.session.email) {
    next();
  }
  else {
    res.redirect('/login');
  }
});

/* GET list kurir page */
router.get('/view-couriers', function (req, res, next) {
	courier.getCouriers(req, res, next).then(function (result) {
		courier.getUnassignedTask(req, res, next).then(function (tasks){
			res.render('view-couriers', { tasks: tasks, couriers: result, title: 'View Couriers', adminName: req.session.fullname, success: req.flash('successCreateCourier'), successDelete: req.flash("successDeleteCourier") });
		}).catch(function (err) {
			res.send(err.message);
		});
	}).catch(function (err) {
		res.send(err.message);
	});
});

/* GET kurir management page. */
router.get('/create-courier', function (req, res, next) {
	courier.getRegions(req,res).then(function(result) {
		res.render('create-courier', { 
			regions: result,
			title: 'Add Couriers', 
			adminName: req.session.fullname,
			button: 'Add', 
			action: '/courier/create-courier', 
			success: req.flash("successCreateCourier"), 
			errors: req.flash("errors"), 
			name: req.flash("name"), 
			phone: req.flash("phone"), 
			region: req.flash("region"),
			errorMessage: req.flash("errorMessage"),
			errorMessageNumber: req.flash("errorMessageNumber")
		});
	}).catch(function(err) {
    res.send(err.message);
  });
});

router.post('/create-courier', function (req, res, next) {
	req.check('name', 'Karakter Name kurang dari 1 atau lebih dari 32').isLength({min:1, max: 32});
	req.check('phone', 'Phone kurang dari 1 angka atau lebih dari 16 angka').isLength({min:1, max: 16});
	req.check('phone', 'Phone hanya boleh angka').isNumeric();
	req.check('region', 'Silahkan pilih Region').notEmpty();

	var errors = req.validationErrors();

	if (errors){
		req.flash('errors',errors);
    req.flash('name',req.body.name);
    req.flash('phone',req.body.phone);
    req.flash('region',req.body.region);
		res.redirect('/courier/create-courier/');
	}
	else{
			req.flash('successCreateCourier',"Courier has been created!");
			courier.addCourier(req, res, next).then(function (Result) {
			res.redirect('/courier/view-couriers');
		}).catch(function (err) {
			res.send(err.message);
		});
	}
});

/* GET courier page by id */

/*Update GET courier without Courier ID parameter*/
router.get('/update-couriers/', function (req, res, next) {
	console.error('Error #602 : Courier id tidak ditemukan');
	req.flash('errorMessage', 'Courier id tidak ditemukan');
	req.flash('errorMessageNumber', 'Error #602');
	res.redirect('/courier/create-courier/');
});

router.get('/update-couriers/:id', function (req, res, next) {
	courier.getCouriersbyID(req, res, next).then(function (result) {
		courier.getRegions(req,res).then(function(allRegions) {
			if (typeof result !== 'undefined' && result.length > 0) {
		    res.render('create-courier', { 
		    	regions: allRegions,
		    	couriers: result, 
		    	title: 'Update Courier', 
		    	adminName: req.session.fullname,
		    	button: 'Update', 
		    	action: '/courier/update-couriers/', 
		    	errors: req.flash("errors")
		    });
			}
			else{
				console.error('Error #602 : Courier id tidak ditemukan');
		    req.flash('errorMessage', 'Courier id tidak ditemukan');
		    req.flash('errorMessageNumber', 'Error #602');
				res.redirect('/courier/create-courier/');
			}
		}).catch(function(err) {
	    res.send(err.message);
	  });
	}).catch(function (err) {
		res.send(err.message);
	});
});

router.post('/update-couriers/:id', function (req, res, next) {
	req.check('name', 'Karakter Name kurang dari 1 atau lebih dari 32').isLength({min:1, max: 32});
	req.check('phone', 'Phone kurang dari 1 angka atau lebih dari 16 angka').isLength({min:1, max: 16});
	req.check('phone', 'Phone hanya boleh angka').isNumeric();
	req.check('region', 'Silahkan pilih Region').notEmpty();

	var errors = req.validationErrors();

	if (errors){
		req.flash("errors", errors);
		res.redirect('/courier/update-couriers/' + req.params.id);
	}
	else{
		req.flash('successCreateCourier',"Courier has been created!");
		courier.getCouriersbyID(req, res, next).then(function (result) {
			if (typeof result !== 'undefined' && result.length > 0) {
				req.body.auth_key = result[0].auth_key;
				req.body.status = result[0].status;
				req.body.last_online = result[0].last_online;
				req.body.latitude = result[0].latitude;
				req.body.longitude = result[0].longitude;
				req.body.region_id = result[0].region_id;
				req.body.password = result[0].password;

				courier.addCourier(req, res, next).then(function (Result) {
					res.redirect('/courier/view-couriers');
				}).catch(function (err) {
					res.send(err.message);
				});
			}
			else{
				res.redirect('/courier/create-courier/');
			}
		}).catch(function (err) {
			res.send(err.message);
		});
	}
});

router.get('/delete/:id', function (req, res, next) {
  courier.deleteCourier(req, res).then(function(Sequelizeresponse){
    if(Sequelizeresponse > 0){
      Sequelizeresponse = "Number of deleted row is "+Sequelizeresponse;
    }
    req.flash('successDeleteCourier',"Courier has been deleted!");
    res.redirect('/courier/view-couriers');
  }).catch(function(err){
    console.log(err.message);
    if (err.message >= 100 && err.message < 600)
      res.sendStatus(err.message);
    else
    res.sendStatus(500);
  });
});

router.get('/assign-task/:courier_id/:task_id/', function (req, res, next) {
  courier.assignTask(req, res).then(function (result) {
    res.redirect('/courier/view-couriers');
  }).catch(function (err) {
    res.send(err.message);
  });
});


module.exports = router;