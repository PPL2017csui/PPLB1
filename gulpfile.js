'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
 

//manual converting
gulp.task('sass', function () {
  return gulp.src('./assets/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./assets/css'));
});
 
//Automatic converting every saved .scss file
gulp.task('sass:watch', function () {
  gulp.watch('./assets/scss/**/*.scss', ['sass']);
});