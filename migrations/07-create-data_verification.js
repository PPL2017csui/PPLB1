"use strict";

module.exports = {
	up: function(queryInterface, Sequelize) {
		return queryInterface.createTable('data_verifications', {
			order_id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				allowNull: false
				references: {
					model: 'tasks',
					key: 'id'
				}
			},
			image: {
				type: Sequelize.TEXT,
				allowNull: false
			},
			signature: {
				type: Sequelize.TEXT,
				allowNull: false
			},
			timestamp: {
				type: Sequelize.DATE,
				allowNull: false
			},
			note: {
				type: Sequelize.TEXT,
			}
		});
	},
	down: function(queryInterface, Sequelize) {
		return queryInterface.dropTable('data_verifications');
	}
};
