module.exports = function(sequelize, DataTypes) {
  var Region = sequelize.define("region", {
  	id: {type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true},
    region : {type: DataTypes.STRING, allowNull: false}
  });

  return Region;
};
