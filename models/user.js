var Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  var Sequelize = require('sequelize');

  var User = sequelize.define("user", {
    id: {type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true},
    fullname: {type: DataTypes.STRING, allowNull: false, unique: true},
  	email: {type: DataTypes.STRING, allowNull: false},
  	password: {type: DataTypes.STRING, allowNull: false},
  	roles_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'role',
        key: 'id',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
      }
    },
  	phone: {type: DataTypes.STRING, allowNull: false},
  	avatar: {type: DataTypes.STRING, allowNull: true},
  	last_online: {type: DataTypes.DATE, allowNull: true}
  });

  return User;
};
