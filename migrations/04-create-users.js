module.exports = {
	up: function(queryInterface, Sequelize) {
		return queryInterface.createTable('users', {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true,
				allowNull: false
			},
			fullname: {
				type: Sequelize.STRING,
				allowNull: false
				unique: true;
			},
			email: {
				type: Sequelize.STRING,
				allowNull: false
			},
			password: {
				type: Sequelize.STRING,
				allowNull: false
			},
			roles_id: {
				type: Sequelize.INTEGER,
				allowNull: false
				references: {
					model: 'roles',
					key: 'id'
				}
			},
			phone: {
				type: Sequelize.STRING,
				allowNull: false
			},
			avatar: {
				type: Sequelize.STRING,
				allowNull: false
			},
			last_online: {
				type: Sequelize.DATE,
				allowNull: false
			}
		});
	},
	down: function(queryInterface, Sequelize) {
		return queryInterface.dropTable('users');
	}
};
