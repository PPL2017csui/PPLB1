module.exports = {
	up: function(queryInterface, Sequelize) {
		return queryInterface.createTable('couriers', {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true,
				allowNull: false
			},
			name: {
				type: Sequelize.STRING,
				allowNull: false
			},
			auth_key: {
				type: Sequelize.STRING,
				allowNull: false
			},
			phone: {
				type: Sequelize.STRING,
				allowNull: false
			},
			region: {
				type: Sequelize.TEXT,
				allowNull: false
			},
			status: {
				type: Sequelize.STRING,
				allowNull: false
			},
			last_online: {
				type: Sequelize.DATE,
				allowNull: false
			},
			latitude: {
				type: Sequelize.STRING,
				allowNull: false
			},
			longitude: {
				type: Sequelize.STRING,
				allowNull: false
			},
			region_id: {
				type: Sequelize.INT,
				allowNull: false,
				references: {
					model: 'regions',
					key: 'id'
				}
			},
			password: {
				type: Sequelize.STRING,
				allowNull: false
			}
		});
	},
	down: function(queryInterface, Sequelize) {
		return queryInterface.dropTable('couriers');
	}
};
