module.exports = {
	up: function(queryInterface, Sequelize) {
		return queryInterface.createTable('regions', {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true,
				allowNull: false
			},
			region: {
				type: Sequelize.STRING,
				allowNull: false
			}
		});
	},
	down: function(queryInterface, Sequelize) {
		return queryInterface.dropTable('regions');
	}
};
