  var Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  var Sequelize = require('sequelize');

  var Courier = sequelize.define("courier", {
    id : {type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true},
    name: {type: DataTypes.STRING, allowNull: false},
    auth_key: {type: DataTypes.STRING, allowNull: false},
    phone: {type: DataTypes.STRING, allowNull: false},
    region: {type: DataTypes.TEXT, allowNull: false},
    status: {type: DataTypes.STRING, allowNull: false},
    last_online: {type: DataTypes.DATE, allowNull: false},
    latitude: {type: DataTypes.STRING, allowNull: false},
    longitude: {type: DataTypes.STRING, allowNull: false},
    region_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'region',
          key: 'id',
          deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
    },
    password: {type: DataTypes.STRING, allowNull: false}
  });
  return Courier;
};