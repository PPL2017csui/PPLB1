require('dotenv').config();
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
var assert = chai.assert;
chai.should();
var underTest = require('../controllers/courier.js');
var Sequelize = require('sequelize');

describe('Courier', function () {
	var req, res, userLength;
	var latestId = 0;
	var date = new Date();
	beforeEach(function() {
    req = { body:{
      name : "Courier test",
			region : "JAKARTA",
			phone :"082211116446"
    }};

    req.params = {};
    res = {send : function(args){}};
  });


 	describe('on add courier', function() {
 		after(function() {
	    return underTest.getCouriers().then(function(users){
	      for (var index in users) {
	        if (users[index].id > latestId) {
	          latestId = users[index].id;
	        }
	      }
	      req.params.id = latestId;
	      return underTest.deleteCourier(req, res).then(function(rows){
	          return Promise.resolve(rows);
	      });
	    });
	  });
    it('should return true when success add courier', function() {
      return underTest.addCourier(req,res).should.eventually.equal(true);
    });
  });

 	describe('on view courier list', function () {
    it('should return true when getting data on view', function () {
      return underTest.getCouriers(req, res).should.eventually.to.be.not.null;
    });
  });

 	describe('on deleting courier', function () {
    it('should return 0 when deleting courier which courier id not available on current courier table', function () {
      req.params.id = 0;
      return underTest.deleteCourier(req, res).should.eventually.equal(0);
    });
    it('should return 1 when deleting courier which courier id available on current courier table', function () {
      return underTest.addCourier(req, res).then(function(createResult){
        return underTest.getCouriers().then(function(users){
          for (var index in users) {
            if (users[index].id > latestId) {
              latestId = users[index].id;
            }
          }
          req.params.id = latestId;
          return underTest.deleteCourier(req, res).then(function(rows){
              return Promise.resolve(rows);
          });
        });
      }).should.eventually.equal(1);
    });
  });

	describe('on updating courier\'s details', function () {

    it('should return false on updating courier', function () {
      return underTest.addCourier(req, res).then(function(result){
        return underTest.getCouriers().then(function(users){
          for (var index in users) {
            if (users[index].id > latestId) {
              latestId = users[index].id;
            }
          }
          req.params.id = latestId;
          req.body.auth_key = "004BCD";
          req.body.status = "ongoing";
          req.body.latitude = "-6.277953";
          req.body.longitude = "106.802902";
          req.body.region_id = 2;
          req.body.password = "pass";
          
          return underTest.addCourier(req, res).then(function(updateResult){
            return underTest.deleteCourier(req, res).then(function(rows){
                return Promise.resolve(updateResult);
            });
          });
        });
      }).should.eventually.equal(false);
    });
  });

  describe('get courier by ID', function () {
  	after(function() {
	    return underTest.getCouriers().then(function(users){
	      for (var index in users) {
	        if (users[index].id > latestId) {
	          latestId = users[index].id;
	        }
	      }
	      req.params.id = latestId;
	      return underTest.deleteCourier(req, res).then(function(rows){
	          return Promise.resolve(rows);
	      });
	    });
	  });
    it('should return true when ID exists', function () {
      return underTest.addCourier(req, res).then(function(result){
        return underTest.getCouriers().then(function(users){
          for (var index in users) {
            if (users[index].id > latestId) {
              latestId = users[index].id;
            }
          }
          req.params.id = latestId;

          return underTest.getCouriersbyID(req, res).should.eventually.to.be.not.null;

        });
      });
    });    
  });

	describe('get unassigned task', function () {
		it('should return true if there is task with value wait for courier assignment', function () {
			var req = {};
			var res = {send : function(args){}};
			return underTest.getUnassignedTask(req, res).should.eventually.to.not.be.null;
		});
	});

	describe('on assigning task to courier', function () {
    it('should not have any database\'s constraint and assigned correctly', function () {
        var req = {
          params : {
            courier_id : 3,
            task_id : 1
          },
          session : {
          	fullname : "WARDOYOK HERMAN"
          }
        }

        var res = {send : function (args) {}}

        return underTest.assignTask(req, res).then(function (result) {
          return Promise.resolve(result);
        }).should.eventually.have.length(1);
    });
  });

	describe('on getting task with associated id', function () {
		it('should return object when get associated task from courier id', function () {
			var courierid = 1;
			var startDate = "2017-06-05 16:00:00+07";
			var endDate = "2017-06-08 16:00:00+07";
			var taskStatus = "success";
			return underTest.getAssociatedTask(courierid, startDate, endDate, taskStatus).should.eventually.to.not.be.null;
		});
	});

	describe('on getting task with associated id', function () {
		it('should return empty array when get associated task from courier id not listed in database', function () {
			var courierid = 1000000;
			var startDate = "2017-06-05 16:00:00+07";
			var endDate = "2017-06-08 16:00:00+07";
			var taskStatus = "success";
			return underTest.getAssociatedTask(courierid, startDate, endDate, taskStatus).then(function(result){
				return assert.equal(0, result.length);
			});
		});
	});

	describe('on getting task with associated id', function () {
		it('should return empty array when get associated task from courier id not listed in database', function () {
			var courierid = 1000000;
			var startDate = "2017-06-05 16:00:00+07";
			var endDate = "2017-06-08 16:00:00+07";
			var taskStatus = "pending";
			return underTest.getAssociatedTask(courierid, startDate, endDate, taskStatus).then(function(result){
				return assert.equal(0, result.length);
			});
		});
	});

	describe('on getting task with associated id', function () {
		it('should return empty array when get associated task from courier id not listed in database', function () {
			var courierid = 1000000;
			var startDate = "2017-06-05 16:00:00+07";
			var endDate = "2017-06-08 16:00:00+07";
			var taskStatus = "ongoing";
			return underTest.getAssociatedTask(courierid, startDate, endDate, taskStatus).then(function(result){
				return assert.equal(0, result.length);
			});
		});
	});

	describe('on getting task with associated id', function () {
		it('should return code 069 when parameter is not complete', function () {
			var courierid;
			var startDate;
			var endDate;
			var taskStatus;
			return underTest.getAssociatedTask(courierid, startDate, endDate, taskStatus).then().catch(function(errcode){
				return assert.equal(609, errcode);
			});
		});
	});
	
  describe('get online courier', function () {
  	after(function() {
	    return underTest.getCouriers().then(function(users){
	      for (var index in users) {
	        if (users[index].id > latestId) {
	          latestId = users[index].id;
	        }
	      }
	      req.params.id = latestId;
	      return underTest.deleteCourier(req, res).then(function(rows){
	          return Promise.resolve(rows);
	      });
	    });
	  });
    it('should return true when courier online exists', function () {
    	req.body.last_online = date;
      return underTest.addCourier(req, res).then(function(result){
        return underTest.getOnlineCouriers().should.eventually.to.be.not.null;
      });
    });    
  });
});

describe('Courier for API', function () {
	describe('on courier authentication', function () {
		it('should return data when success get ucourier with specific name', function () {
			var req = {username: 'EMON'}
			return underTest.courierAuthenticationForApi(req).should.eventually.to.not.be.null;
		});

		it('should return null when there is no courier name', function () {
			var req = {username: 'CEO cermati'}
			return underTest.courierAuthenticationForApi(req).should.eventually.null;
		});
	});
});