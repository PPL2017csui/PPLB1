require('dotenv').config();
var chai = require('chai');
var chaiThings = require('chai-things');
chai.should();
chai.use(chaiThings);
var assert  = chai.assert;
var underTest = require('../controllers/document.js');
var courier = require('../models').courier;
var Promise = require('bluebird');
var Sequelize = require('sequelize');
var sinon = require('sinon');

describe('API Document List', function () {
	it('should returns at least 1 document', function () {
    return underTest.getDocumentsForApi().then(function (result) {
      result.should.have.length.above(1);
    });
	});

  it('should returns documents with attribute id', function () {
    return underTest.getDocumentsForApi().then(function (result) {
      result.should.contain.a.thing.with.property('id');
    });
  });

  it('should returns documents with attribute name', function () {
    return underTest.getDocumentsForApi().then(function (result) {
      result.should.contain.a.thing.with.property('name');
    });
  });

  it('should returns documents with attribute task_id', function () {
    return underTest.getDocumentsForApi().then(function (result) {
      result.should.contain.a.thing.with.property('task_id');
    });
  });

  it('should returns documents with attribute quantity', function () {
    return underTest.getDocumentsForApi().then(function (result) {
      result.should.contain.a.thing.with.property('quantity');
    });
  });

  it('should returns documents with attribute description', function () {
    return underTest.getDocumentsForApi().then(function (result) {
      result.should.contain.a.thing.with.property('description');
    });
  });

  //

	it('should returns at least 1 document', function () {
		var req = {params:{
			id : 1
		}};
		var res = {};
    return underTest.getDocumentsbyIdForApi(req, res).then(function (result) {
      result.should.have.length(1);
    });
	});

  it('should returns documents with attribute id', function () {
		var req = {params:{
			id : 1
		}};
		var res = {};
    return underTest.getDocumentsbyIdForApi(req, res).then(function (result) {
      result.should.contain.a.thing.with.property('id');
    });
  });

  it('should returns documents with attribute name', function () {
		var req = {params:{
			id : 1
		}};
		var res = {};
    return underTest.getDocumentsbyIdForApi(req, res).then(function (result) {
      result.should.contain.a.thing.with.property('name');
    });
  });

  it('should returns documents with attribute task_id', function () {
		var req = {params:{
			id : 1
		}};
		var res = {};
    return underTest.getDocumentsbyIdForApi(req, res).then(function (result) {
      result.should.contain.a.thing.with.property('task_id');
    });
  });

  it('should returns documents with attribute quantity', function () {
		var req = {params:{
			id : 1
		}};
		var res = {};
    return underTest.getDocumentsbyIdForApi(req, res).then(function (result) {
      result.should.contain.a.thing.with.property('quantity');
    });
  });

  it('should returns documents with attribute description', function () {
		var req = {params:{
			id : 1
		}};
		var res = {};
    return underTest.getDocumentsbyIdForApi(req, res).then(function (result) {
      result.should.contain.a.thing.with.property('description');
    });
  });

  //

  it('should returns at least 1 document', function () {
    var req = {body:{
      name: 'Kartu ATM'
    }};
    var res = {};
    return underTest.getDocumentsByNameForApi(req, res).then(function (result) {
      result.should.have.length(1);
    });
  });

  it('should returns documents with attribute id', function () {
    var req = {body:{
      name: 'Kartu ATM'
    }};
    var res = {};
    return underTest.getDocumentsByNameForApi(req, res).then(function (result) {
      result.should.contain.a.thing.with.property('id');
    });
  });

  it('should returns documents with attribute name', function () {
    var req = {body:{
      name: 'Kartu ATM'
    }};
    var res = {};
    return underTest.getDocumentsByNameForApi(req, res).then(function (result) {
      result.should.contain.a.thing.with.property('name');
    });
  });

  it('should returns documents with attribute task_id', function () {
    var req = {body:{
      name: 'Kartu ATM'
    }};
    var res = {};
    return underTest.getDocumentsByNameForApi(req, res).then(function (result) {
      result.should.contain.a.thing.with.property('task_id');
    });
  });

  it('should returns documents with attribute quantity', function () {
    var req = {body:{
      name: 'Kartu ATM'
    }};
    var res = {};
    return underTest.getDocumentsByNameForApi(req, res).then(function (result) {
      result.should.contain.a.thing.with.property('quantity');
    });
  });

  it('should returns documents with attribute description', function () {
    var req = {body:{
      name: 'Kartu ATM'
    }};
    var res = {};
    return underTest.getDocumentsByNameForApi(req, res).then(function (result) {
      result.should.contain.a.thing.with.property('description');
    });
  });

});
