var model = require('../models');
var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));
var path  = require('path');

var getDocumentsForApi = function (req, res) {
  return model.document.findAll({
  	raw:true,
  	attributes: ['id', 'name', 'task_id', 'quantity', 'description']
  });
}

var getDocumentsbyIdForApi = function (req, res) {
  return model.document.findAll({
  	raw:true,
  	attributes: ['id', 'name', 'task_id', 'quantity', 'description'],
    where : {
      task_id : req.params.id
    }
  });
}

var getDocumentsByNameForApi = function (req, res) {
  return model.document.findAll({
    raw:true,
    attributes: ['id', 'name', 'task_id', 'quantity', 'description'],
    where : {
      name : req.body.name
    }
  });
}

module.exports = {
    getDocumentsForApi : getDocumentsForApi,
    getDocumentsbyIdForApi  :  getDocumentsbyIdForApi,
    getDocumentsByNameForApi : getDocumentsByNameForApi
};
