require('dotenv').config();
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
var assert = chai.assert;
chai.should();
var underTest = require('../controllers/authentication.js');
var Sequelize = require('sequelize');

describe('Authentication', function() {
	describe('on login', function() {
		it('should return data when success login', function() {
			var req = { body:{
				email: 'nurhidayat@gmail.com',
				password: 'dayat123'
			}};
			var res = {};

			return underTest.login(req, res).should.eventually.not.to.be.empty;
		});

		it('should return null when failed login', function() {
			var req = {body:{
				email: 'false_admin',
				password: 'false_admin'
			}};
			var res = {};

			return underTest.login(req, res).should.eventually.to.be.null;
		});
	});

	describe('on logout', function() {
		it('should return true when success logout', function() {
			var req = {body: {}};
			var res = {};
			
			return underTest.logout(req, res).should.to.be.true;
		});
	});
});