var Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  var Sequelize = require('sequelize');

  var Task = sequelize.define("task", {
    id: {type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true},
  	name: {type: DataTypes.STRING, allowNull: false},
  	description: {type: DataTypes.TEXT, allowNull: false},
  	address: {type: DataTypes.TEXT, allowNull: false},
  	region: {type: DataTypes.TEXT, allowNull: false},
  	deadline: {type: DataTypes.DATE, allowNull: false},
  	task_status: {type: DataTypes.STRING, allowNull: false},
  	latitude: {type: DataTypes.STRING, allowNull: false},
    longitude: {type: DataTypes.STRING, allowNull: false},
    category: {type: DataTypes.INTEGER, allowNull: true},
    courier_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'courier',
        key: 'id',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
      }
    },
    assigned_by: {
      type: DataTypes.STRING,
      allowNull: false,
      references: {
        model: 'user',
        key: 'fullname',
        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
      }
    },
    pickedUpAt: {type: DataTypes.DATE, allowNull: true},
    finishedAt: {type: DataTypes.DATE, allowNull: true}
  }, {
    associate: function(models) {
      Task.hasMany(models.document, { onDelete: 'cascade', hooks: true });
    },
  });

  return Task;
};
