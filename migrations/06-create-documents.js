module.exports = {
	up: function(queryInterface, Sequelize) {
		return queryInterface.createTable('documents', {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true,
				allowNull: false
			},
			name: {
				type: Sequelize.STRING,
				allowNull: false
			},
			task_id: {
				type: Sequelize.INTEGER,
				allowNull: false
				references: {
					model: 'tasks',
					key: 'id'
				}
			},
			quantity: {
				type: Sequelize.INTEGER,
				allowNull: false
				defaultValue: 1
			},
			description: {
				type: Sequelize.TEXT,
				allowNull: false
			}
		});
	},
	down: function(queryInterface, Sequelize) {
		return queryInterface.dropTable('documents');
	}
};
