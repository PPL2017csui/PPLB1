var express = require('express');
var router = express.Router();
var taskControllers = require('../controllers/task.js');
var courierControllers = require('../controllers/courier.js');
var fs = require('fs');
var path  = require('path');
var multer = require('multer');
var storage = multer.diskStorage({
  destination: 'uploads/',
  filename: function (req, file, cb) {
    cb(null, new Date().toString()+"-"+file.originalname);
  },
  limits : {
    filesize : 5000
  }
});
var fs = require('fs');
var mime = require('mime');
var upload = multer({
  storage : storage
});

router.use(function(req, res, next) {
  if(req.session.email) {
    next();
  }
  else {
    res.redirect('/login');
  }
});

router.get('/view', function (req, res, next) {
  taskControllers.getTasks(req, res).then(function (tasks) {
    courierControllers.getCouriers(req, res).then(function (couriers) {
      taskControllers.getDocuments(req, res).then(function (documents) {
        res.render('view-tasks', {
          title: 'View Task',
          adminName: req.session.fullname,
          tasks : tasks,
          couriers : couriers,
          documents : documents,
          success: req.flash('success'),
        });
      }).catch(function (err) {
        res.send(err.message);
      });
    }).catch(function (err) {
      res.send(err.message);
    });
  }).catch(function (err) {
    res.send(err.message);
  })
});

router.get('/create', function(req, res, next) {
  courierControllers.getRegions(req,res).then(function(result) {
    res.render('create-task', {
      regions: result,
      title: 'Create Task',
      submitvalue: 'Create Task',
      adminName: req.session.fullname,
      actionForm: '/task/create',
      name: req.flash('name'),
      description: req.flash('description'),
      address: req.flash('address'),
      region: req.flash('region'),
      deadline: req.flash('deadline'),
      error: req.flash('errors'),
      documentname: req.flash('documentname'),
      arrayOfErrors: req.flash('arrayOfErrors')
    });
  }).catch(function(err) {
    res.send(err.message);
  });
});

router.post('/create', function(req, res, next){
  req.check('name', 'karakter Task Name kurang dari 1 atau lebih dari 32').isLength({min:1, max: 32});
  req.check('description', 'description tidak boleh kosong').notEmpty();
  req.check('address', 'address tidak boleh kosong').notEmpty();
  req.check('region', 'region tidak boleh kosong').notEmpty();
  req.check('deadline','deadline tidak boleh kosong').notEmpty();
  if(req.body.document){
    for (var i = 0; i < req.body.document.length; i++) {
      req.check('document['+i+'][name]','nama dokumen tidak boleh kosong').notEmpty();
      req.check('document['+i+'][quantity]','jumlah dokumen tidak boleh kosong').notEmpty();
      req.check('document['+i+'][quantity]','quantity hanya boleh angka').isNumeric();
      req.check('document['+i+'][description]','deskripsi dokumen tidak boleh kosong').notEmpty();
    }
  }

  var errors = req.validationErrors();

  if(errors) {
    req.flash('errors',errors);
    req.flash('name',req.body.name);
    req.flash('description',req.body.description);
    req.flash('address',req.body.address);
    req.flash('region',req.body.region);
    req.flash('deadline',req.body.deadline);
    if(req.body.document && req.body.document.length > 0){
      var arrayOfErrors = [];
      for (var i = 0; i < req.body.document.length; i++) {
        arrayOfErrors.push({
            name: req.body.document[i].name,
            quantity: req.body.document[i].quantity,
            description: req.body.document[i].description
        });
      }
      req.flash('arrayOfErrors',arrayOfErrors);
    }
    res.redirect('/task/create');
  }
  else {
    req.body.assigned_by = req.session.fullname;
    taskControllers.createTask(req, res).then(function(InsertionStatus){
      req.flash('success','Task has been created!');
      res.redirect('view');
    }).catch(function(err){
      res.send(err.message);
    });
  }
});

router.get('/view', function (req, res, next) {
  taskControllers.getTasks(req, res).then(function (tasks) {
    courierControllers.getCouriers(req, res).then(function (couriers) {
      res.render('view-tasks', {
        tasks : tasks,
        couriers : couriers,
        adminName: req.session.fullname,
        success : req.flash('success')
      });
    });
  });
});

router.get('/update/:id', function (req, res, next) {
  taskControllers.getTaskById(req, res).then(function (result) {
    courierControllers.getRegions(req, res).then(function (allRegions) {
      taskControllers.getTaskDocumentById(req, res).then(function (documents) {
        if (typeof result !== 'undefined' && result.length > 0) {
          res.render('create-task', {
            regions: allRegions,
            title: 'Update Task',
            adminName: req.session.fullname,
            submitvalue: 'Update Task',
            actionForm: '/task/update/' + req.params.id,
            name: result[0].name,
            description: result[0].description,
            address: result[0].address,
            region: result[0].region,
            deadline: result[0].deadline,
            latitude: result[0].latitude,
            longitude: result[0].longitude,
            documents : documents,
            current_task_id : req.params.id,
            error: req.flash('errors'),
            success: req.session.success,
            updateForm: 'updateTask',
            arrayOfErrors: req.flash('arrayOfErrors')
          });
          req.session.errors = null;
          req.session.success = null;
        }
        else{
          res.redirect('/user/create-user/');
        }
      }).catch(function (err) {
        res.send(err.message);
      });
    }).catch(function (err) {
      res.send(err.message);
    });
  }).catch(function (err) {
    res.send(err.message);
  });
});

router.post('/update/:id', function (req, res, next) {
  taskControllers.getTaskById(req, res, next).then(function (oldTask) {
    req.body.task_status = oldTask[0].task_status; 
    req.check('name', 'karakter Task Name kurang dari 1 atau lebih dari 32').isLength({min:1, max: 32});
    req.check('description', 'description tidak boleh kosong').notEmpty();
    req.check('address', 'address tidak boleh kosong').notEmpty();
    req.check('region', 'region tidak boleh kosong').notEmpty();
    req.check('deadline','deadline tidak boleh kosong').notEmpty();
    if(req.body.document){
      for (var i = 0; i < req.body.document.length; i++) {
        req.check('document['+i+'][name]','nama dokumen tidak boleh kosong').notEmpty();
        req.check('document['+i+'][quantity]','jumlah dokumen tidak boleh kosong').notEmpty();
        req.check('document['+i+'][quantity]','quantity hanya boleh angka').isNumeric();
        req.check('document['+i+'][description]','deskripsi dokumen tidak boleh kosong').notEmpty();
      }
    }

    var errors = req.validationErrors();

    if (errors){
      req.flash('errors',errors);
      req.flash('name',req.body.name);
      req.flash('description',req.body.description);
      req.flash('address',req.body.address);
      req.flash('region',req.body.region);
      req.flash('deadline',req.body.deadline);
      if(req.body.document){
        var arrayOfErrors = [];
        for (var i = 0; i < req.body.document.length; i++) {
          arrayOfErrors.push({
              name: req.body.document[i].name,
              quantity: req.body.document[i].quantity,
              description: req.body.document[i].description
          });
        }
        req.flash('arrayOfErrors',arrayOfErrors);
      }
      res.redirect('/task/update/' + req.params.id);
    }
    else{
      req.body.assigned_by = req.session.fullname;
      taskControllers.updateTask(req, res).then(function(InsertionStatus){
        req.flash('success','Task has been updated!');
        res.redirect('/task/view');
      }).catch(function(err){
        res.send(err.message);
      });
    }
  });
});

router.get('/delete/:id', function (req, res, next) {
  taskControllers.deleteTask(req, res).then(function(Sequelizeresponse){
    res.redirect('/task/view');
  }).catch(function(err){
    var message = "OOPS THERE IS SOMETHING WRONG WHEN DELETING TASK";
    var status = err.status;
    var stack = err.stack;
    var time = new Date(Date.now());
    var log = "error time "+time+"\n"+"error message: "+err.message+"\n"+"error status: "+err.status+"\n"+"error stack: "+err.stack+"\n";
    fs.appendFile('logs/error.log', log, function(err){
      if(err) {
          return console.log(err);
      }
      res.render('error', {
        message : message,
        status : status,
        stack : stack
      });
    });
  });
});

router.get('/assign-to/:task_id/:courier_id', function (req, res, next) {
  taskControllers.assignTaskToCourier(req, res).then(function (result) {
    res.redirect('/task/view');
  });
});

router.get('/upload', function(req, res, next) {
  res.render('upload-task', {
    title : "Upload task",
    adminName: req.session.fullname,
    error : req.flash('error')
  });
});

router.get('/template-file', function(req, res, next){
    res.download('uploads/valid.json', 'task-template.json');
});

router.post('/upload', upload.single('taskfile'), function(req, res, next) {
  taskControllers.readJsonFile(req, res).then(function(tasks){
      taskControllers.createAllTask(tasks, res).then(function(data){
        req.flash('success', 'Task Created Successfully!');
        res.redirect('/task/view');
      }).catch(function(err){
        req.flash('error', 'Invalid JSON Structure!');
        res.redirect('/task/upload');
      });
  }).catch(function(err){
    req.flash('error', 'File corrupted, file size too large or file extentions not supported!');
    res.redirect('/task/upload');
  });
});

module.exports = router;
