# Cermati Courier System
[![coverage report](https://gitlab.com/PPL2017csui/PPLB1/badges/develop/coverage.svg)](https://gitlab.com/PPL2017csui/PPLB1/commits/develop)
---
## Description

This project creates a system for the couriers of PT. Dwi Cermat Indonesia ([Cermati.com](https://www.cermati.com/))

## Installation

To start development process, you need to setup the projects on your local environments.  
Setup your local environments by runnning the following command:

```
# Clone the repository and go to the root directory
git clone https://gitlab.com/PPL2017csui/PPLB1.git
cd PPLB1

# Install all necessary dependencies
npm install

# Set environment variables
cp .env.template .env
```

## Documentation
To run the app, simply run the command `npm start` from the root directory.
To run, automatic compiler for scss to css, simply run `gulp sass:watch`

### Directory Structure
```
├── assets
│   ├── css
│   ├── fonts
│   ├── img
│   └── js
├── bin
│   └── www
├── routes
│   └── index.js
├── views
│   ├── error.hbs
│   ├── index.hbs
│   └── main.hbs
├── .env.template
├── .gitignore
├── README.md
├── app.js
├── package.json
```
### Directories
`assets` stores all of the additional files such as stylesheets, scripts, images, etc.  
`bin` stores all binaries executables.  
`routes` stores all files for routing purposes.  
`views` stores all files for user interfaces.   
### Core Files
`www` is the executables used to start the server.  
`app.js` is the file containing the configuration of how the server and the app behave.  
`routes/index.js` is the main file to set all routes.  
`views/error.hbs` is the file to show any error occured throughout the app.  
`views/main.hbs` is the main view for templating.
### Project Error Code

The list of error code in this project.

| Error Code | Description |
| :---: |:---|
| Error #400 | Bad Request |
| Error #403 | Forbidden |
| Error #404 | Not Found |
| Error #500 | Internal Server Error |
| Error #502 | Bad Gateway |
| Error #601 | Task id tidak ditemukan |
| Error #602 | Courier id tidak ditemukan |
| Error #603 | User id tidak ditemukan |
| Error #604 | File extention not matched |
| Error #605 | Failed Task tidak ditemukan |
| Error #606 | on Progress Task tidak ditemukan |
| Error #607 | Successful Task tidak ditemukan |
| Error #608 | Task yang Dibuat tidak ditemukan |
| Error #609 | Parameter kurir statistik tidak lengkap |
| Error #610 | File extentions not support |
| Error #611 | Corrupted JSON Type |
| Error #612 | JSON Structured not valid |
| Error #613 | Task empty on JSON file |

## Contributors

This projects consists a list of contributors:  
@dimejune13  
@asepwhite  
@fajrinajiseno  
@meonster.yi  
@fathinrhmn  
