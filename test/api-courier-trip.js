require('dotenv').config();
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
var assert  = chai.assert;
chai.should();
var underTest = require('../controllers/courier.js');
var courier = require('../models').courier;
var Promise = require('bluebird');
var Sequelize = require('sequelize');
var sinon = require('sinon');

describe('API Courier Trip', function () {

	describe('/POST trip start', function () {
		it('should POST courier_trip completely', function () {
			var req = { body : {
					task_id : 1,
					courier_id : 1
				}	
			}

			var res = {send : function (args) {}};
			return Promise.resolve(underTest.courierOnTrip(req, res)).should.eventually.deep.equal([1]);
			
		});

		it('should not POST courier_trip without task_id', function () {
			var req = { body : {
					courier_id : 1
				}	
			}

			var res = {send : function (args) {}};
			return Promise.resolve(underTest.courierOnTrip(req, res)).should.eventually.deep.equal([1]);
		});

		it('should not POST courier_trip without courier_id', function () {
			var req = { body : {
					task_id : 1
				}	
			}

			var res = {send : function (args) {}};
			return Promise.resolve(underTest.courierOnTrip(req, res)).should.eventually.deep.equal([0]);
		});

		it('should not POST courier_trip without parameters', function () {
			var req = { body : {

				}	
			}

			var res = {send : function (args) {}};
			return Promise.resolve(underTest.courierOnTrip(req, res)).should.eventually.deep.equal([0]);
		});

	});

	describe('/POST trip cancel', function () {
		it('should POST courier_trip completely', function () {
			var req = { body : {
				task_id : 1,
				courier_id : 1,
				reason : 'I\'m sick'
				}	
			}

			var res = {send : function (args) {}};
			return Promise.resolve(underTest.courierCancelTrip(req, res)).should.eventually.deep.equal([1]);
		});

		it('should not POST courier_trip without task_id', function () {
			var req = { body : {
				courier_id : 1,
				reason : 'I\'m sick'
				}	
			}

			var res = {send : function (args) {}};
			return Promise.resolve(underTest.courierCancelTrip(req, res)).should.eventually.deep.equal([1]);
		});

		it('should not POST courier_trip without courier_id', function () {
			var req = { body : {
				task_id : 1,
				reason : 'I\'m sick'
				}	
			}

			var res = {send : function (args) {}};
			return Promise.resolve(underTest.courierCancelTrip(req, res)).should.eventually.deep.equal([0]);
		});

		it('should not POST courier_trip without reason', function () {
			var req = { body : {
				task_id : 1,
				courier_id : 1
				}	
			}

			var res = {send : function (args) {}};
			return Promise.resolve(underTest.courierCancelTrip(req, res)).should.eventually.deep.equal([1]);
		});

		it('should not POST courier_trip without parameters', function () {
			var req = { body : {

				}	
			}

			var res = {send : function (args) {}};
			return Promise.resolve(underTest.courierCancelTrip(req, res)).should.eventually.deep.equal([0]);
		});

	});

});
