module.exports = function(sequelize, DataTypes) {
  var Role = sequelize.define("role", {
    id: {type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true},
    role_name: {type: DataTypes.STRING, allowNull: false}
  });
  return Role;
};
