module.exports = {
	up: function(queryInterface, Sequelize) {
		return queryInterface.createTable('roles', {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true,
				allowNull: false
			},
			role_name: {
				type: Sequelize.STRING,
				allowNull: false
			}
		});
	},
	down: function(queryInterface, Sequelize) {
		return queryInterface.dropTable('roles');
	}
};
