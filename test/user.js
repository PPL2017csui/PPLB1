require('dotenv').config();
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
var assert = chai.assert;
chai.should();
var underTest = require('../controllers/user.js');
var Sequelize = require('sequelize');

describe('User', function () {
  var req, res, userLength;
  var latestId = 0;
  beforeEach(function() {
    req = { body:{
      fullname: "test name",
      email: "test email",
      password: "test password",
      roles: 1,
      phone: "test phone",
      avatar: "/img/avatar/default.png",
      last_online: "2017-01-17 15:00:00+07"
    }};

    req.params = {};
    res = {send : function(args){}};
  });

  describe('on user roles', function () {
    it('should return true when getting data on view', function () {
      return underTest.getRoles(req, res).should.eventually.to.be.not.null;
    });
  });

  describe('on view users', function () {
    it('should return true when getting data on view', function () {
      return underTest.getUsers(req, res).should.eventually.to.be.not.null;
    });
    it('should return true when getting roles', function () {
      return underTest.getRoles(req, res).should.eventually.to.be.not.null;
    });

    it('should return true when getting data on view', function () {
      return underTest.noUsers(req, res).should.eventually.to.be.empty;
    });
  });

  describe('on create user', function() {
    it('should return true when success creating user', function() {
      return underTest.createUser(req,res).should.eventually.true;
    });
  });

  describe('get user by ID', function () {
    it('should return true when ID exists', function () {
      return underTest.createUser(req, res).then(function(result){
        return underTest.getUsers().then(function(users){
          for (var index in users) {
            if (users[index].id > latestId) {
              latestId = users[index].id;
            }
          }
          req.params.id = latestId;

          return underTest.getUserById(req, res).should.eventually.to.be.not.null;

        });
      });
    });
    it('should return empty when ID not exists', function () {
      req.params.id = 0;
      return underTest.getUserById(req, res).should.eventually.to.be.empty;
    });
  });

  describe('on updating user\'s details', function () {
      it('should return false on updating user', function () {
        return underTest.createUser(req, res).then(function(result){
          return underTest.getUsers().then(function(users){
            for (var index in users) {
              if (users[index].id > latestId) {
                latestId = users[index].id;
              }
            }
            req.params.id = latestId;

            return underTest.updateUser(req, res).then(function(updateResult){
              return underTest.deleteUser(req, res).then(function(rows){
                  return Promise.resolve(updateResult);
              });
            });
          });
        }).should.eventually.equal(false);
      });
  });

  describe('on deleting user', function () {
    it('should return 0 when deleting user which user id not available on current users table', function () {
      req.params.id = 0;
      return underTest.deleteUser(req, res).should.eventually.equal(0);
    });
    it('should return 1 when deleting user which user id available on current users table', function () {
      return underTest.createUser(req, res).then(function(createResult){
        return underTest.getUsers().then(function(users){
          for (var index in users) {
            if (users[index].id > latestId) {
              latestId = users[index].id;
            }
          }
          req.params.id = latestId;
          return underTest.deleteUser(req, res).then(function(rows){
              return Promise.resolve(rows);
          });
        });
      }).should.eventually.equal(1);
    });
  });
});
