var express = require('express');
var router = express.Router();
var taskControllers = require('../../controllers/task.js');
var documentControllers = require('../../controllers/document.js');
var courierControllers = require('../../controllers/courier.js');

router.post('/add-task', function(req, res) {
	taskControllers.createTask(req, res).then(function (result) {
		res.json(result);
	}).catch(function (err) {
		res.send(err.message);
	});
});		

router.get('/documents', function(req, res, next) {
	documentControllers.getDocumentsForApi(req, res).then(function (result) {
		var jsonResult = {
	      status: 200,
	      documents: result
	    };

		res.json(jsonResult);
	});
});

router.post('/courier-auth', function(req, res) {
	courierControllers.courierAuthenticationForApi(req).then(function (result) {
		var jsonResult;
		
		if (result) {
			jsonResult = {
				status: 200,
				result: 'authorized'
			}
		}
		else {
			jsonResult = {
				status: 401,
				result: 'unauthorized'
			}
		}
	});
});

router.get('/documents/:id', function(req, res, next) {
	documentControllers.getDocumentsbyIdForApi(req, res).then(function (result) {
		var jsonResult = {
	      status: 200,
	      documents: result
	    };

		res.json(jsonResult);
	});
});

router.post('/trip/start', function (req, res) {
	courierControllers.courierOnTrip(req, res).then(function (result) {
		var json_result = returnStatus(result, 0);
		res.json(json_result);
	}).catch(function (err) {
		console.error('Error #400 : Bad Request for POST /trip/start');
		res.send(err.message);
	});
});

router.post('/trip/cancel', function (req, res) {
	courierControllers.courierCancelTrip(req, res).then(function (result) {
		var json_result = returnStatus(result, 0);
		res.json(json_result);
	}).catch(function (err) {
		console.error('Error #400 : Bad Request for POST /trip/cancel');
		res.send(err.message);
	});
});

router.get('/tasks', function (req, res) {
	taskControllers.getTasksForApi().then(function (tasks) {
		var json_result = returnStatus(tasks, 1)
		res.json(json_result);
	}).catch(function (err) {
		console.error('Error #400 : Bad Request for GET /tasks');
		res.send(err.message);
	});
});

router.post('/location', function (req, res) {
	courierControllers.courierUpdateLocation(req, res).then(function (result) {
		var json_result = returnStatus(result, 0);
		res.json(json_result);
	}).catch(function (err) {
		console.error('Error #400 : Bad Request for POST /location');
		res.send(err.message);
	});
});



function returnStatus(result, data) {
	if (data == 0) {
		if (result == 1)
			return {status : 200};
		else
			return {status : 400};
	} else {
		return {
			status : 200,
			tasks : result
		}
	}	
}

module.exports = router;