var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var dotenv = require('dotenv').config();
var expressValidator = require('express-validator');
var expressSession = require('express-session');
var flash = require('connect-flash');
var fs = require('fs');
var hbs = require('hbs');
var flash = require('connect-flash');

hbs.registerHelper('equal', require('handlebars-helper-equal'));
hbs.registerHelper("debug", function(optionalValue) {
  console.log("Current Context");
  console.log("====================");
  console.log(this);

  if (optionalValue) {
    console.log("Value");
    console.log("====================");
    console.log(optionalValue);
  }
});
hbs.registerHelper('ifCond', function (v1, v2, options) {
  if (v1 === v2) {
    return options.fn(this);
  }
  return options.inverse(this);
});
hbs.registerHelper("math", function(lvalue, operator, rvalue, options) {
    lvalue = parseFloat(lvalue);
    rvalue = parseFloat(rvalue);
        
    return {
        "+": lvalue + rvalue,
        "-": lvalue - rvalue,
        "*": lvalue * rvalue,
        "/": lvalue / rvalue,
        "%": lvalue % rvalue
    }[operator];
});
hbs.registerHelper( 'concat', function(index, documentName) {
    return 'document['+index+']['+documentName+']';
});

var app = express();
var index = require('./routes/index');
var task = require('./routes/task');
var courier = require('./routes/courier');
var user = require('./routes/user');
var statistic = require('./routes/statistic');
var api_v1 = require('./routes/v1/api');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.set('view options', { layout: 'main'} );


// uncomment after placing your favicon in /assets
//app.use(favicon(path.join(__dirname, 'assets', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'assets')));
app.use(expressSession({secret: 'max', saveUninitialized: false, resave: false}));
app.use(flash());

app.use('/', index);
app.use('/task', task);
app.use('/courier', courier);
app.use('/user', user);
app.use('/api/v1', api_v1);
app.use('/statistic', statistic);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


module.exports = app;