var model = require('../models');
var Sequelize = require('sequelize');
var Promise = require('bluebird');
var firebaseController = require('./firebase');

var getCouriers = function(req, res, next) {
    return model.courier.findAll();
}

var getCouriersbyID = function(req, res, next) {
    return model.courier.findAll({
        where: {
          id: req.params.id
        }
    });
}
var getOnlineCouriers = function(req, res, next) {
    var MINIMUMMINUTES = 10;
    var SECONDTOMINUTES = 60000;
    return model.courier.findAll({
        where: {
          last_online : {
            $gt: new Date(new Date() - MINIMUMMINUTES * SECONDTOMINUTES)
          }
        }
    });
}

var addCourier = function(req, res, next) {
    var courierInstance = CreateCourierInstance(req);
    return model.courier.upsert(courierInstance);
}

var getRegions = function(req, res) {
    return model.region.findAll();
}
function CreateCourierInstance(req){
  var MINIMUMMINUTES = 11;
  var SECONDTOMINUTES = 60000;
  var date = new Date(new Date() - MINIMUMMINUTES * SECONDTOMINUTES);
  var courierInstance = {
    name: req.body.name,
    region: req.body.region,
    phone: req.body.phone
  };

  if (req.params.id) {
   courierInstance.id = req.params.id;
  }
  if (req.body.auth_key) {
   courierInstance.auth_key = req.body.auth_key;
  }
  else {
    courierInstance.auth_key = 'auth';
  }

  if (req.body.status) {
    courierInstance.status = req.body.status;
  }
  else {
    courierInstance.status = 'available';
  }

  if (req.body.last_online) {
    courierInstance.last_online = req.body.last_online;
  }
  else{
    courierInstance.last_online = date;
  }

  if (req.body.latitude) {
    courierInstance.latitude = req.body.latitude;
  }
  else{
    courierInstance.latitude = '-6.178368';
  }

  if (req.body.longitude) {
    courierInstance.longitude = req.body.longitude;
  }
  else{
    courierInstance.longitude = '106.7862343';
  }

  if (req.body.region_id) {
    courierInstance.region_id = req.body.region_id;
  }
  else{
    courierInstance.region_id = 1;
  }

  if (req.body.password) {
   courierInstance.password = req.body.password;
  }
  else {
    courierInstance.password = 'password';
  }
  return courierInstance;
};

var deleteCourier = function(req, res){
  return model.courier.destroy({
    where: {
      id : req.params.id
    }
  });
};

var assignTask = function(req, res) {
  var courier_id = req.params.courier_id;
  var task_id = req.params.task_id;

  var assignTask = {
    courier_id : courier_id,
    task_status : "not done",
    assigned_by : req.session.fullname,
    updatedAt : new Date(Date.now())
  }

  var condition = {
    where : { id : task_id}
  }

  model.courier.findAll({
    where : {
      id : courier_id
    }
  }).then(function (result) {

    var auth_key = result[0].auth_key;
    console.log(auth_key);

    if(auth_key != null)
      firebaseController.sendNotificationToUser(auth_key, 'Tugas Baru', 'Anda mendapat tugas baru');

  });

  return model.task.update(assignTask, condition);
};

var getUnassignedTask = function(req, res) {
  return model.task.findAll({
    where: {
      task_status: "pending"
    }
 });
};

var courierAuthenticationForApi = function(req, res) {
  return model.courier.findOne({
    where: {
      name: req.username
    }
  });
}
var getAssociatedTask = function(courierId, startDate, endDate, taskStatus) {
  if(!courierId || !startDate || !endDate || !taskStatus){
    return Promise.reject(609);
  }

  var timeEvent;
  if (taskStatus === "not done") {
    timeEvent = "updatedAt";
  } else if (taskStatus === "ongoing") {
    timeEvent = "pickedUpAt";
  }
  else {
    timeEvent = "finishedAt";
  }

  return model.task.findAll({
    where: {
      courier_id: courierId,
      [timeEvent] : {
        $gte :startDate,
        $lte :endDate
      },
      task_status: taskStatus
    },
    order : '"'+[timeEvent]+'" ASC'
  });
};

var courierOnTrip = function (req, res) {
  var courier_id = req.body.courier_id;
  var task_id = req.body.task_id;

  var data = {
    task_status : 'ongoing',
    pickedUpAt : new Date(Date.now())
  }

  var condition = {
    where : {id : task_id}
  }

  model.task.update(data, condition);

  data = {
    status : 'on a trip'
  }

  condition = {
    where : {id : courier_id}
  }

  return model.courier.update(data, condition);
}

var courierCancelTrip = function (req, res) {
  var courier_id = req.body.courier_id;
  var task_id = req.body.task_id;

  var data = {
    task_status : 'failed',
    finishedAt : new Date(Date.now())
  }

  var condition = {
    where : {id : task_id}
  }

  model.task.update(data, condition);

  data = {
    status : 'available'
  }

  condition = {
    where : {id : courier_id}
  }

  return model.courier.update(data, condition);
}

var courierUpdateLocation = function (req, res) {
  var courier_id = req.body.courier_id;
  var lat = req.body.lat;
  var lon = req.body.lon;
  var data = {
    last_online : new Date(Date.now()),
    latitude : lat,
    longitude : lon,
  }

  var condition = {
    where : {id : courier_id}
  }

  return model.courier.update(data, condition);

}

module.exports = {
    getCouriers: getCouriers,
    getCouriersbyID: getCouriersbyID,
    addCourier: addCourier,
    deleteCourier : deleteCourier,
    assignTask : assignTask,
    getUnassignedTask : getUnassignedTask,
    courierAuthenticationForApi : courierAuthenticationForApi,
    getAssociatedTask : getAssociatedTask,
    courierOnTrip : courierOnTrip,
    courierCancelTrip : courierCancelTrip,
    getRegions: getRegions,
    getUnassignedTask: getUnassignedTask,
    getOnlineCouriers : getOnlineCouriers,
    courierUpdateLocation : courierUpdateLocation
};
