var express = require('express');
var router = express.Router();
var authentication = require("../controllers/authentication.js");
var courier = require('../controllers/courier.js');
var sha1 = require('sha1');

router.get('/login', function(req, res, next) {
  if(!req.session.email) {
    res.render('login', { title: 'Portal login Cermati\'s Courier System', layout: '', email: req.flash('email'), error: req.flash('error') });
  } else {
    res.redirect('/');
  }
});

router.post('/login', function(req, res, next) {
  authentication.login(req, res).then(function(result) {
    if (result) {
      if (result.password == sha1(req.body.password)) {
        req.session.id = result.id;
        req.session.fullname = result.fullname;
        req.session.email = result.email;
        req.session.roles = result.roles;
        res.redirect('/');
      }
      else {
        req.flash('email',req.body.email);
        req.flash('error','Password salah');
        res.redirect('/login');
      }
    }
    else {
      req.flash('error','Email salah')
      res.redirect('/login');
    }
  }).catch(function (err) {
    res.send(err.message);
  });
});

/* GET home page. */
router.get('/', function(req, res, next) {
  if(req.session.email) {
    courier.getOnlineCouriers(req, res, next).then(function (result) {
      res.render('index', { title: 'Cermati\'s Courier System', adminName: req.session.fullname, subtitle: 'Dashboard', couriers: result });
    }).catch(function (err) {
      res.send(err.message);
    });
  }
  else {
    res.redirect('login');
  }
});

router.get('/logout', function(req, res, next) {
  if(req.session.email) {
    req.session.destroy(function(err){
      res.redirect('/login');
    });
  }
  else {
    res.redirect('login');
  }
});

module.exports = router;