require('dotenv').config();
var chai = require('chai');
var chaiThings = require('chai-things');
chai.should();
chai.use(chaiThings);
var assert  = chai.assert;
var underTest = require('../controllers/task.js');
var courier = require('../models').courier;
var Promise = require('bluebird');
var Sequelize = require('sequelize');
var sinon = require('sinon');

describe('API Task List', function () {
	it('should returns at least 1 task', function () {
    return Promise.resolve(underTest.getTasksForApi()).should.eventually.have.length.above(1);
	});

  it('should returns task with attribute id', function () {
    return underTest.getTasksForApi().then(function (result) {
      result.should.contain.a.thing.with.property('id');
    });
  });

  it('should returns task with attribute name', function () {
    return underTest.getTasksForApi().then(function (result) {
      result.should.contain.a.thing.with.property('name');
    });
  });

  it('should returns task with attribute courier_id', function () {
    return underTest.getTasksForApi().then(function (result) {
      result.should.contain.a.thing.with.property('courier_id');
    });
  });

  it('should returns task with attribute address', function () {
    return underTest.getTasksForApi().then(function (result) {
      result.should.contain.a.thing.with.property('address');
    });
  });

  it('should returns task with attribute region', function () {
    return underTest.getTasksForApi().then(function (result) {
      result.should.contain.a.thing.with.property('region');
    });
  });
});
