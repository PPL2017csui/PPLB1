var courier_id = 1;

function setCourierId(id) {
  courier_id = id;
}

$(document).ready(function() {

  // Set last page opened on the menu
  $('#menu a[href]').on('click', function() {
    sessionStorage.setItem('menu', $(this).attr('href'));
  });

  if (!sessionStorage.getItem('menu')) {
    $('#menu #dashboard').addClass('active');
  } else {
    // Sets active and open to selected page in the left column menu.
    $('#menu a[href=\'' + sessionStorage.getItem('menu') + '\']').parents('li').addClass('active open');
  }

  // Sidebar state
  if (localStorage.getItem('column-left') == 'active') {
    $('#button-menu i').replaceWith('<i class="fa fa-arrow-left fa-lg"></i>');
    $('#column-left').addClass('active');

    // Slide Down Menu
    $('#menu li.active').has('ul').children('ul').addClass('collapse in');
    $('#menu li').not('.active').has('ul').children('ul').addClass('collapse');
  } else {
    $('#button-menu i').replaceWith('<i class="fa fa-arrow-right fa-lg"></i>');

    $('#menu li li.active').has('ul').children('ul').addClass('collapse in');
    $('#menu li li').not('.active').has('ul').children('ul').addClass('collapse');
  }

  // Menu button
  $('#button-menu').on('click', function() {
    // Checks if the left column is active or not.
    if ($('#column-left').hasClass('active')) {

      localStorage.setItem('column-left', '');

      $('#button-menu i').replaceWith('<i class="fa fa-arrow-right fa-lg"></i>');

      $('#column-left').removeClass('active');

      $('#menu > li > ul').removeClass('in collapse');
      $('#menu > li > ul').removeAttr('style');
    } else {

      localStorage.setItem('column-left', 'active');

      $('#button-menu i').replaceWith('<i class="fa fa-arrow-left fa-lg"></i>');

      $('#column-left').addClass('active');

      // Add the slide down to open menu items
      $('#menu li.open').has('ul').children('ul').addClass('collapse in');
      $('#menu li').not('.open').has('ul').children('ul').addClass('collapse');
    }
  });

  // Menu
  $('#menu').find('li').has('ul').children('a').on('click', function() {
    if ($('#column-left').hasClass('active')) {
      $(this).parent('li').toggleClass('open').children('ul').collapse('toggle');
      $(this).parent('li').siblings().removeClass('open').children('ul.in').collapse('hide');
    } else if (!$(this).parent().parent().is('#menu')) {
      $(this).parent('li').toggleClass('open').children('ul').collapse('toggle');
      $(this).parent('li').siblings().removeClass('open').children('ul.in').collapse('hide');
    }
  });

  //Other Task Type Input
  $("#other-task-type").hide();
  $('#input-task-type').change(function () {
        var value = $(this).val();

        if (value == "lain-lain") {
          $("#other-task-type").show();
        } else {
          $("#other-task-type").hide();
        }
    });

  $('.datetime').datetimepicker({
    pickDate: true,
    pickTime: true
  });

  $('.form-group.required').find('input').focus(function() {
    $(this).removeClass('noInput');
      if ($(this).parent().attr('class') === 'input-group'){
        $(this).parent().parent().find('.text-danger').html('');
      } else {
        $(this).parent().find('.text-danger').html('');
      }

    });
    $('.form-group.required').find('.input-group-btn').click(function() {
    $(this).prev().removeClass('noInput');
    $(this).parent().next().html('');
    });


  $(".assign-task").on("click", function(e) {
    var uid = $(this).data('id');
    x0p({
        title: 'Confirmation',
        text: 'Are you sure?',
        icon: 'warning',
        animationType: 'fadeIn',
        buttons: [
            {
                type: 'cancel'
            },
            {
                type: 'info',
                text: 'confirm',
                showLoading: true
            }
        ]
    }, function(button) {
        if(button == 'info') {
            // Simulate Delay
            setTimeout(function() {
              location.href="/courier/assign-task/"+courier_id+"/"+uid;
            }, 2000);
        }
    });
    e.preventDefault();
  });


  $(".delete-courier").on("click", function(e) {
    var uid = $(this).data('id');

    x0p({
        title: 'Confirmation',
        text: 'Are you sure?',
        icon: 'warning',
        animationType: 'fadeIn',
        buttons: [
            {
                type: 'cancel'
            },
            {
                type: 'info',
                text: 'confirm',
                showLoading: true
            }
        ]
    }, function(button) {
        if(button == 'info') {
            // Simulate Delay
            setTimeout(function() {
              location.href="/courier/delete/" + uid;
            }, 2000);
        }
    });
    e.preventDefault();
  });

  $("#addButtonCourier").on("click", function(e) {
    var form = $(this).parents('form');

    x0p({
        title: 'Confirmation',
        text: 'Are you sure?',
        icon: 'warning',
        animationType: 'fadeIn',
        buttons: [
            {
                type: 'cancel'
            },
            {
                type: 'info',
                text: 'confirm',
                showLoading: true
            }
        ]
    }, function(button) {
        if(button == 'info') {
            // Simulate Delay
            setTimeout(function() {
              form.submit();
            }, 2000);
        }
    });
    e.preventDefault();
  });

  $(".delete-user").on("click", function(e) {
    var uid = $(this).data('id');

    x0p({
        title: 'Confirmation',
        text: 'Are you sure?',
        icon: 'warning',
        animationType: 'fadeIn',
        buttons: [
            {
                type: 'cancel'
            },
            {
                type: 'info',
                text: 'confirm',
                showLoading: true
            }
        ]
    }, function(button) {
        if(button == 'info') {
            // Simulate Delay
            setTimeout(function() {
              location.href="/user/delete/" + uid;
            }, 2000);
        }
    });
    e.preventDefault();
  });

  $(".user-details").on("click", function(e) {
    var uid = $(this).data('id');
    var data = {}
    data.id = uid;
    $.ajax({
      type: 'POST',
      url: "/user/view-userDetails",
      async: true,
      data: data,
      dataType: "json",
      success: function (data) {
        $("#userModal .userImage").attr({src:data.avatar, alt:data.avatar});
        $("#userModal .userId").html(data.id);
        $("#userModal .userFullname").html(data.fullname);
        $("#userModal .userEmail").html(data.email);
        $("#userModal .userRolesId").html(data.roles_id);
        $("#userModal .userPhone").html(data.phone);
        $("#userModal").modal({show:true});
      },
      error: function (data) { alert("gagal")}
    });

  });

  $(".task-details").on("click", function(e) {
    var uid = $(this).data('id');
    var data = {}
    data.id = uid;
    $.ajax({
      type: 'POST',
      url: "/user/view-userDetails",
      async: true,
      data: data,
      dataType: "json",
      success: function (data) { 
        $("#userModal .userImage").attr({src:data.avatar, alt:data.avatar});
        $("#userModal .userId").html(data.id);
        $("#userModal .userFullname").html(data.fullname);
        $("#userModal .userEmail").html(data.email);
        $("#userModal .userRolesId").html(data.roles_id);
        $("#userModal .userPhone").html(data.phone);
        $("#userModal").modal({show:true});
      },
      error: function (data) { alert("gagal")}
    });

  });

  $(".deletetask").on("click", function(e) {
    var uid = $(this).data('id');

    x0p({
        title: 'Confirmation',
        text: 'Are you sure?',
        icon: 'warning',
        animationType: 'fadeIn',
        buttons: [
            {
                type: 'cancel'
            },
            {
                type: 'info',
                text: 'confirm',
                showLoading: true
            }
        ]
    }, function(button) {
        if(button == 'info') {
            // Simulate Delay
            setTimeout(function() {
              x0p({
                title: 'Success',
              text: 'Task has been deleted',
              icon: 'ok',
              buttons: [],
              autoClose: 3000
              });
              setTimeout(function() {
                location.href="/task/delete/" + uid;
              }, 3000);
            }, 2000);
        }
        if(button == 'cancel') {
            x0p('Canceled',
                'No task deleted',
                'error', false);
          }
    });
    e.preventDefault();
  });

  $(".createButton").on("click", function(e) {
    var form = $(this).parents('form');

    x0p({
        title: 'Confirmation',
        text: 'Are you sure?',
        icon: 'warning',
        animationType: 'fadeIn',
        buttons: [
            {
                type: 'cancel'
            },
            {
                type: 'info',
                text: 'confirm',
                showLoading: true
            }
        ]
    }, function(button) {
        if(button == 'info') {
            // Simulate Delay
            setTimeout(function() {
              form.submit();
            }, 2000);
        }
    });
    e.preventDefault();
  });

  // tooltips on hover
  $('[data-tooltip=\'tooltip\']').tooltip({container: 'body'});

  // Makes tooltips work on ajax generated content
  $(document).ajaxStop(function() {
    $('[data-tooltip=\'tooltip\']').tooltip({container: 'body'});
  });

  $('#taskfile').bind('change', function() {
    var fileSize = this.files[0].size;
    var maxSize = 5000000;
    if (fileSize > maxSize) {
      alert("File size exceed 5 MB!");
      window.location = "/task/upload";
    }

  });
});