var model = require('../models');
var sequelize = require('sequelize');
var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));
var path  = require('path');
var firebaseController = require('./firebase');

var createTask = function(req, res) {
  var taskInstance = createTaskInstance(req);
  return model.task.upsert(taskInstance).then(function (insertResult) {
    return getLastTaskId(req, res).then(function (last) {
      if (req.body.document && req.body.document.length != 0) {
        for (var i = 0; i < req.body.document.length; i++) {
          var name = req.body.document[i].name;
          var quantity = req.body.document[i].quantity;
          var description = req.body.document[i].description;
          var documentInstance =  createDocumentInstance(name, last, quantity, description);
          model.document.upsert(documentInstance);
        }
      }
      return Promise.resolve(insertResult);
    });
  });
};

var getTasks = function(req, res) {
  return model.task.findAll();
}

var getTaskById = function(req, res) {
  return model.task.findAll({
    where: {
      id: req.params.id
    }
 });
}

var getTaskCreatedByDate = function(startDate, endDate) {
  return model.task.findAll({
    where: {
      createdAt: {
        $gte :startDate,
        $lte :endDate
      }
    }
 });
}

var getSuccessfulTasks = function(req, res) {
  return model.task.findAll({
    where: {
      task_status: "success"
    }
 });
}

var getSuccessfulTasksByDate = function(startDate, endDate) {
  return model.task.findAll({
    where: {
      task_status: "success",
      finishedAt: {
        $gte :startDate,
        $lte :endDate
      }
    }
 });
}

var getOnProgressTasks = function(req, res) {
  return model.task.findAll({
    where: {
      task_status: "ongoing"
    }
 });
}

var getDocuments = function(req, res) {
  return model.document.findAll();
}

var getTaskDocumentById = function(req, res) {
  return model.document.findAll({
    where: {
      task_id: req.params.id
    }
 });
}

var getOnProgressTasksByDate = function(startDate, endDate) {
  return model.task.findAll({
    where: {
      task_status: "ongoing",
      pickedUpAt: {
        $gte :startDate,
        $lte :endDate
      }
    }
 });
}

var getFailedTasks = function(req, res) {
  return model.task.findAll({
    where: {
      task_status: "failed"
    }
 });
}

var getFailedTasksByDate = function(startDate, endDate) {
  return model.task.findAll({
    where: {
      task_status: "failed",
      finishedAt: {
        $gte :startDate,
        $lte :endDate
      }
    }
 });
}

var getLastTaskId = function(req, res) {
  return model.task.max('id');
}

var updateTask = function(req, res){
  var taskInstance = createTaskInstance(req);
  if (req.params.id) {
     var thisID = req.params.id;
  }
  return model.task.upsert(taskInstance).then(function (updateResult) {
    if (thisID) {
      model.document.destroy({where: {task_id: thisID} });
    }

    if (req.body.document && req.body.document.length != 0) {
      for (var i = 0; i < req.body.document.length; i++) {
        var name = req.body.document[i].name;
        var quantity = req.body.document[i].quantity;
        var description = req.body.document[i].description;
        var documentInstance =  createDocumentInstance(name, thisID, quantity, description);
        model.document.upsert(documentInstance);
      }
    }
    return Promise.resolve(updateResult);
  });
}

var deleteTask = function(req, res){
  return model.task.destroy({
    where: {
      id : req.params.id
    }
  });
}

var deleteTaskDocument = function(req, res){
  return model.document.destroy({
    where: {
      task_id: req.params.id
    }
  });
}

var assignTaskToCourier = function(req, res) {
  var courier_id = req.params.courier_id;
  var task_id = req.params.task_id;

  var assignTask = {
    courier_id : courier_id,
    task_status : "not done",
    assigned_by : req.session.fullname,
    updatedAt : new Date(Date.now())
  }

  var condition = {
    where : { id : task_id}
  }

  model.courier.findAll({
    where : {
      id : courier_id
    }
  }).then(function (result) {

    var auth_key = result[0].auth_key;
    console.log(auth_key);

    if(auth_key != null)
      firebaseController.sendNotificationToUser(auth_key, 'Tugas Baru', 'Anda mendapat tugas baru');

  });

  return model.task.update(assignTask, condition);
}

var readJsonFile = function(req, res){
  var fileSize = req.file.size;
  var fileSizeLimit = 5000000;
  var ext = path.extname(req.file.originalname);
  if (fileSize > fileSizeLimit) {
      return Promise.reject(false);
  }
  if (ext === '.json') {
    return fs.readFileAsync(req.file.path, 'utf8').then(function(data){
      try {
        var Jsondata = JSON.parse(data);
        return Promise.resolve(Jsondata);
      } catch (err) {
        console.log("611");
        return Promise.reject(false);
      }
     });
  } else {
    console.log("610");
    var err = {message : "File extentions not supported!", error:{status : "500 Internal server error", stack:""}};
    return Promise.reject(false);
  }
}

var validateJsonStructure = function(Jsondata) {
  var tasks = Jsondata.tasks;
  if (tasks && tasks.length > 0) {
    for (var index in tasks) {
      var currentTask = tasks[index];
      if (currentTask.name && currentTask.description && currentTask.address && currentTask.region && currentTask.deadline && currentTask.latitude && currentTask.longitude && currentTask.document) {
        return Promise.resolve(true);
      } else {
        console.log("612");
        return Promise.reject(false);
      }
    }
  } else {
    console.log("613");
    return Promise.reject(false);
  }
}

var createAllTask = function(jsonData, res){
   var tasks = jsonData.tasks;
   var promises = [];
   var temp = {body : {}};
   for (var index in tasks) {
     temp.body = tasks[index];
     promises.push(createTask(temp, res).then(function(result){
       response = result;
     }));
   }
  return Promise.all(promises).then(function(){
    return Promise.resolve(response);
  });
}

function createTaskInstance(req){
  if(!req.body.latitude){
    req.body.latitude = 0;
  }
  if(!req.body.longitude){
    req.body.longitude = 0;
  }
  if(!req.body.courier_id){
    req.body.courier_id = 1;
  }
  if(!req.body.assigned_by){
    req.body.assigned_by = "WARDOYOK HERMAN";
  }
  if(!req.body.task_status){
    req.body.task_status = "pending";
  }

  var taskInstance = {
    name: req.body.name,
    description: req.body.description,
    address: req.body.address,
    region: req.body.region,
    deadline: req.body.deadline,
    task_status: req.body.task_status,
    latitude: req.body.latitude,
    longitude: req.body.longitude,
    courier_id: req.body.courier_id,
    assigned_by: req.body.assigned_by
  };
  if (req.params) {
    taskInstance.id = req.params.id;
  }
  if(req.body.pickedUpAt) {
    taskInstance.pickedUpAt = req.body.pickedUpAt;
  }
  if(req.body.finishedAt) {
    taskInstance.finishedAt = req.body.finishedAt;
  }

  return taskInstance;
};

function createDocumentInstance(name, task_id, quantity, description){
  var documentInstance = {
    name: name,
    task_id : task_id,
    quantity: quantity,
    description: description
  };
  return documentInstance;
};

var getTasksForApi = function (req, res) {
  return model.task.findAll({
    raw:true,
    attributes: { exclude: ['createdAt', 'updatedAt'] }
  });
}

module.exports = {
    createTask: createTask,
    getTasks: getTasks,
    updateTask : updateTask,
    deleteTask : deleteTask,
    getTaskById : getTaskById,
    assignTaskToCourier : assignTaskToCourier,
    readJsonFile : readJsonFile,
    createAllTask : createAllTask,
    validateJsonStructure : validateJsonStructure,
    getSuccessfulTasks : getSuccessfulTasks,
    getOnProgressTasks : getOnProgressTasks,
    getFailedTasks : getFailedTasks,
    getTaskCreatedByDate: getTaskCreatedByDate,
    getSuccessfulTasksByDate: getSuccessfulTasksByDate,
    getOnProgressTasksByDate: getOnProgressTasksByDate,
    getFailedTasksByDate: getFailedTasksByDate,
    getLastTaskId : getLastTaskId,
    getDocuments : getDocuments,
    getTaskDocumentById : getTaskDocumentById,
    deleteTaskDocument : deleteTaskDocument,
    getTasksForApi : getTasksForApi
};
