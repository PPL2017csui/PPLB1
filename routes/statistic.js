var express = require('express');
var router = express.Router();
var taskControllers = require('../controllers/task.js');
var courierControllers = require('../controllers/courier.js');


router.use(function(req, res, next) {
  if(req.session.email) {
    next();
  }
  else {
    res.redirect('/login');
  }
});

router.get('/task', function (req, res, next) {
  taskControllers.getTasks(req, res).then(function (taskCreated) {
    taskControllers.getSuccessfulTasks(req, res).then(function (successfulTasks) {
      taskControllers.getOnProgressTasks(req, res).then(function (onProgressTasks) {
        taskControllers.getFailedTasks(req, res).then(function (failedTasks) {
          res.render('task-statistics', {
            taskCreated : taskCreated.length,
            adminName: req.session.fullname,
            successfulTasks : successfulTasks.length,
            onProgressTasks : onProgressTasks.length,
            failedTasks : failedTasks.length,
            startDate : req.flash('startDate'),
            endDate : req.flash('endDate'),
            success : req.flash('success'),
            errors: req.flash('errors'),
            success : req.flash('success')
          });
        }).catch(function (err) {
          console.error("Error #605 : Failed Task tidak ditemukan");
          res.send(err.message);
        });
      }).catch(function (err) {
        console.error("Error #606 : on Progress Task tidak ditemukan");
        res.send(err.message);
      });
    }).catch(function (err) {
      console.error("Error #607 : Successful Task tidak ditemukan");
      res.send(err.message);
    });
  }).catch(function (err) {
      console.error("Error #608 : Task yang dibuat tidak ditemukan");
      res.send(err.message);
  });
});

router.post('/task', function (req,res,next) {
  startDate = req.body.startDate;
  endDate = req.body.endDate;
  req.check('startDate', 'invalid date').isDate();
  req.check('endDate', 'invalid date').isDate();

  var errors = req.validationErrors();
  if (errors){
    req.flash('errors',errors);
    req.flash('startDate', startDate);
    req.flash('endDate', endDate);
    res.redirect('/statistic/task');
  } else {
    req.flash('startDate', startDate);
    req.flash('endDate', endDate);

    taskControllers.getTaskCreatedByDate(startDate, endDate).then(function (taskCreated) {
      taskControllers.getSuccessfulTasksByDate(startDate, endDate).then(function (successfulTasks) {
        taskControllers.getOnProgressTasksByDate(startDate, endDate).then(function (onProgressTasks) {
          taskControllers.getFailedTasksByDate(startDate, endDate).then(function (failedTasks) {
            res.render('task-statistics', {
              taskCreated : taskCreated.length,
              adminName: req.session.fullname,
              successfulTasks : successfulTasks.length,
              onProgressTasks : onProgressTasks.length,
              failedTasks : failedTasks.length
            });
          }).catch(function (err) {
            console.error("Error #605 : Failed Task tidak ditemukan");
            res.send(err.message);
          });
        }).catch(function (err) {
          console.error("Error #606 : on Progress Task tidak ditemukan");
          res.send(err.message);
        });
      }).catch(function (err) {
        console.error("Error #607 : Successful Task tidak ditemukan");
        res.send(err.message);
      });
    }).catch(function (err) {
        console.error("Error #608 : Task yang dibuat tidak ditemukan");
        res.send(err.message);
    });
  }
});

router.get('/courier', function (req, res, next) {
  courierControllers.getCouriers().then(function(couriers){
    res.render('courier-statistics',{
      couriers : couriers,
      adminName: req.session.fullname
    });
  });
});

router.post('/courier', function (req, res, next) {
  var data = [];
  var labels = [];
  var titleLable = " ";
  var courierId = req.body.courierId;
  var startDate = req.body.startDate;
  var endDate =  req.body.endDate;
  var taskStatus =  req.body.taskStatus;
  var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  var taskSum = 0;
  var prevTime = new Date(startDate);
  var endTime = new Date(endDate);
  var timeEvent;
  if (taskStatus === "not done") {
    timeEvent = "updatedAt";
  } else if (taskStatus === "ongoing") {
    timeEvent = "pickedUpAt";
  }
  else {
    timeEvent = "finishedAt";
  }
  var basis = req.body.viewBasis;
  var label;
  if (basis === "daily") {
    label = prevTime.getDate()+""+monthNames[prevTime.getMonth()]+prevTime.getFullYear();
  } else if (basis === "monthly") {
    label = monthNames[prevTime.getMonth()]+prevTime.getFullYear();
  }
  else {
    label = prevTime.getFullYear();
  }

  courierControllers.getAssociatedTask(courierId, startDate, endDate, taskStatus).then(function(tasks){
    for (var index in tasks) {
      var taskTime = new Date(tasks[index][timeEvent]);
      if (basis === "daily") {
        while(prevTime.toDateString() !== taskTime.toDateString()) {
          labels.push(label);
          data.push(taskSum);
          prevTime.setDate(prevTime.getDate()+1);
          label = prevTime.getDate()+""+monthNames[prevTime.getMonth()]+prevTime.getFullYear();
          taskSum = 0;
        }
      } else if (basis === "monthly") {
        while(prevTime.getMonth() !== taskTime.getMonth() || prevTime.getFullYear() !== taskTime.getFullYear()) {
          labels.push(label);
          data.push(taskSum);
          prevTime.setMonth(prevTime.getMonth()+1);
          label = monthNames[prevTime.getMonth()]+prevTime.getFullYear();
          taskSum = 0;
        }
      }
      else{
        while(prevTime.getFullYear() !== taskTime.getFullYear()) {
          labels.push(label);
          data.push(taskSum);
          prevTime.setFullYear(prevTime.getFullYear()+1);
          label = prevTime.getFullYear();
          taskSum = 0;
        }
      }
      taskSum++;
    }
    labels.push(label);
    data.push(taskSum);
    if (basis === "daily") {
      prevTime.setDate(prevTime.getDate()+1);
      while(endTime > prevTime){
        label = prevTime.getDate()+""+monthNames[prevTime.getMonth()]+prevTime.getFullYear();
        taskSum = 0;
        prevTime.setDate(prevTime.getDate()+1);
        labels.push(label);
        data.push(taskSum);
      }
    } else if (basis === "monthly") {
      prevTime.setMonth(prevTime.getMonth()+1);
      while(endTime > prevTime){
        label = monthNames[prevTime.getMonth()]+prevTime.getFullYear();
        taskSum = 0;
        prevTime.setMonth(prevTime.getMonth()+1);
        labels.push(label);
        data.push(taskSum);
      }
    }
    else {
      prevTime.setFullYear(prevTime.getFullYear()+1);
      while(endTime > prevTime){
        label = prevTime.getFullYear();
        taskSum = 0;
        prevTime.setFullYear(prevTime.getFullYear()+1);
        labels.push(label);
        data.push(taskSum);
      }
    }
    courierControllers.getCouriers().then(function(couriers){
      var dateRange = new Date(startDate).toDateString()+" - "+new Date(endDate).toDateString();
      titleLable = taskStatus.toUpperCase()+" TASK from "+dateRange;
      var courierIdRaw = req.body.courierId;
      var taskStatusRaw = req.body.taskStatus;
      var viewBasisRaw = req.body.viewBasis;
      var startDateRaw = req.body.startDate;
      var endDateRaw = req.body.endDate;
      res.render('courier-statistics',{
        couriers : couriers,
        data : data,
        label : labels,
        titleLable : titleLable,
        courierIdRaw : courierIdRaw,
        taskStatusRaw : taskStatusRaw,
        viewBasisRaw : viewBasisRaw,
        startDateRaw : startDateRaw,
        endDateRaw : endDateRaw
      });
    });
  });
});

module.exports = router;
