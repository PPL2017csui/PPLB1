var model = require('../models');
var Sequelize = require('sequelize');
var Promise = require('bluebird');
var sha1 = require('sha1');

function createUserInstance(req){
  var userInstance = {
    fullname: req.body.fullname,
    email: req.body.email,
    password: sha1(req.body.password),
    roles_id: req.body.roles,
    phone: req.body.phone,
    avatar: req.body.avatar,
    last_online: '2017-01-17 15:00:00+07'
  };

  if (req.params.id) {
    userInstance.id = req.params.id;
  }
  return userInstance;
};

var getRoles = function(req, res) {
  return model.role.findAll();
};

var createUser = function(req, res) {
  var userInstance = createUserInstance(req);
  return model.user.upsert(userInstance);
};

var getUsers = function(req, res, next) {
  return model.user.findAll();
};

var getUserById = function(req, res, next) {
  return model.user.findAll({
    where: {
      id: req.params.id
    }
 });
};

var updateUser = function(req, res){
  var userInstance = createUserInstance(req);
  return model.user.upsert(userInstance);
}

var noUsers = function(req, res, next) {
  return model.user.findAll({ where: { id: 0 } })
};

var deleteUser = function(req, res){
  return model.user.destroy({
    where: {
      id : req.params.id
    }
  });
}

module.exports = {
  getUsers: getUsers,
  getUserById : getUserById,
  createUser: createUser,
  getRoles: getRoles,
  updateUser : updateUser,
  deleteUser : deleteUser,
  noUsers : noUsers
};
