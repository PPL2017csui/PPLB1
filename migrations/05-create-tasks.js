module.exports = {
	up: function(queryInterface, Sequelize) {
		return queryInterface.createTable('tasks', {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true,
				allowNull: false
			},
			name: {
				type: Sequelize.STRING,
				allowNull: false
			},
			description: {
				type: Sequelize.TEXT,
				allowNull: false
			},
			address: {
				type: Sequelize.TEXT,
				allowNull: false
			},
			region: {
				type: Sequelize.TEXT,
				allowNull: false
			},
			deadline: {
				type: Sequelize.DATE,
				allowNull: false
			},
			task_status: {
				type: Sequelize.STRING,
				allowNull: false
			},
			latitude: {
				type: Sequelize.STRING,
				allowNull: false
			},
			longitude: {
				type: Sequelize.STRING,
				allowNull: false
			},
			courier_id: {
				type: Sequelize.INTEGER,
				allowNull: false
				references: {
					model: 'couriers',
					key: 'id'
				}
			},
			assigned_by: {
				type: Sequelize.STRING,
				allowNull: false
				references: {
					model: 'users',
					key: 'fullname'
				}
			},
		});
	},
	down: function(queryInterface, Sequelize) {
		return queryInterface.dropTable('tasks');
	}
};
