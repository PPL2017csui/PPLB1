require('dotenv').config();
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
var assert = chai.assert;
chai.should();
var underTest = require('../controllers/task.js');
var Sequelize = require('sequelize');
var Promise = require('bluebird');

describe('task', function() {
  var req, res, taskLength;
  var latestId = 0;

  var deleteLatestTask = function(){
    latestId = 0;
    return underTest.getTasks().then(function(tasks){
        for (var index in tasks) {
          if (tasks[index].id > latestId) {
            latestId = tasks[index].id;
          }
        }
        req.params.id = latestId;
        return underTest.deleteTask(req, res);
    });
  }

  beforeEach(function() {
    req = {body:{
      name: "COBA BIKIN TASK",
      description: "test description",
      address: "test adress",
      region: "test region",
      deadline: "2017-01-17 15:00:00+07",
      task_status: "ongoing",
      latitude: "0",
      longitude: "0",
      courier_id: 1,
      assigned_by: "WARDOYOK HERMAN",
      document:[{
        name : "Kartu Kredit Motor",
        quantity : 3,
        description : "Barang berharga"
      }]
    }};
    req.params = {};
    res = {send : function(args){}};
  });

  describe('on view tasks', function () {
    it('should return not null when getting data on view', function () {
      return underTest.getTasks(req, res).should.eventually.to.be.not.null;
    });

    it('should return not null when get document task', function () {
      return underTest.getDocuments(req, res).should.eventually.to.be.not.null;
    });
  });

  describe('get task by ID', function () {
    after(function() {
      return underTest.getTasks().then(function(tasks){
        for (var index in tasks) {
          if (tasks[index].id > latestId) {
            latestId = tasks[index].id;
          }
        }
        req.params.id = latestId;
        return underTest.deleteTask(req, res).then(function(rows){
            return Promise.resolve(rows);
        });
      });
    });

    it('should return true when ID exists', function () {
      return underTest.createTask(req, res).then(function(result){
        return underTest.getTasks().then(function(tasks){
          for (var index in tasks) {
            if (tasks[index].id > latestId) {
              latestId = tasks[index].id;
            }
          }
          req.params.id = latestId;
          return deleteLatestTask().then(function(deleteResponse){
            return underTest.getTaskById(req, res).should.eventually.to.be.not.null;
          });
        });
      });
    });
  });

  describe('get document task by task ID', function () {
    after(function() {
      return underTest.getTasks().then(function(tasks){
        for (var index in tasks) {
          if (tasks[index].id > latestId) {
            latestId = tasks[index].id;
          }
        }
        req.params.id = latestId;
        return underTest.deleteTask(req, res).then(function(rows){
            return Promise.resolve(rows);
        });
      });
    });

    it('should return true when task document exists', function () {
      return underTest.createTask(req, res).then(function(result){
        return underTest.getDocuments().then(function(documents){
          for (var index in documents) {
            if (documents[index].id > latestId) {
              latestId = documents[index].id;
            }
          }
          req.params.id = latestId;

          return underTest.getTaskDocumentById(req, res).should.eventually.to.be.not.null;

        });
      });
    });
  });

  describe('on delete document task by task ID', function () {
    after(function() {
      return underTest.getTasks().then(function(tasks){
        for (var index in tasks) {
          if (tasks[index].id > latestId) {
            latestId = tasks[index].id;
          }
        }
        req.params.id = latestId;
        return underTest.deleteTask(req, res).then(function(rows){
            return Promise.resolve(rows);
        });
      });
    });

    it('should return 0 when deleting document task when document with selected task id not available on current document table', function () {
      req.params.id = 0;
      return underTest.deleteTaskDocument(req, res).should.eventually.equal(0);
    });
  });

  describe('get task created by date', function () {
    it('should return true when getting task created by date', function () {
      return underTest.createTask(req, res).then(function(result) {
        startDate = "2000-01-17 12:00:00+07";
        endDate = "2050-01-17 12:00:00+07";
        return underTest.getTaskCreatedByDate(startDate, endDate).then(function(result){
          deleteLatestTask();
          return result;
        }).should.eventually.to.be.not.empty;
      });
    });
  });

  describe('get successful tasks', function () {
    it('should return true when getting successful tasks', function () {
      req.body.task_status = "success";
      return underTest.createTask(req, res).then(function(result){
        return underTest.getSuccessfulTasks(req, res).then(function(result) {
          deleteLatestTask();
          return result;
        }).should.eventually.to.be.not.null;
      });
    });
  });

  describe('get successful tasks by date', function () {
    it('should return true when getting successful tasks by date', function () {
      req.body.task_status = "success"
      req.body.finishedAt = "2017-11-03 12:00:00+07";
      return underTest.createTask(req, res).then(function(result) {
        startDate = "2017-11-03 12:00:00+07";
        endDate = "2017-11-03 12:00:00+07";
        return underTest.getSuccessfulTasksByDate(startDate, endDate).then(function(result){
          deleteLatestTask();
          return result;
        }).should.eventually.to.be.not.empty;
      });
    });
  });

  describe('get on-progress tasks', function () {
    it('should return true when getting on-progress tasks', function () {
      req.body.task_status = "ongoing";
      return underTest.createTask(req, res).then(function(result){
        return underTest.getOnProgressTasks(req, res).then(function (result) {
          deleteLatestTask();
          return result;
        }).should.eventually.to.be.not.empty;
      });
    });
  });

  describe('get on progress tasks by date', function () {
    it('should return true when getting on progress tasks by date', function () {
      req.body.task_status = "ongoing"
      req.body.pickedUpAt = "2017-11-03 12:00:00+07";
      return underTest.createTask(req, res).then(function(result) {
        startDate = "2017-11-03 12:00:00+07";
        endDate = "2017-11-03 12:00:00+07";
        return underTest.getOnProgressTasksByDate(startDate, endDate).then(function (result) {
          deleteLatestTask();
          return result;
        }).should.eventually.to.be.not.empty;
      });
    });
  });

  describe('get failed tasks', function () {
    it('should return true when getting failed tasks', function () {
      req.body.task_status = "failed";
      return underTest.createTask(req, res).then(function(result){
        return underTest.getFailedTasks(req, res).then(function (result) {
          deleteLatestTask();
          return result;
        }).should.eventually.to.be.not.empty;
      });
    });
  });

  describe('get failed tasks by date', function () {
    it('should return true when getting failed tasks by date', function () {
      req.body.task_status = "failed"
      return underTest.createTask(req, res).then(function(result) {
        startDate = "2000-01-17 12:00:00+07";
        endDate = "2050-01-17 12:00:00+07";
        return underTest.getFailedTasksByDate(startDate, endDate).then(function (result) {
          deleteLatestTask();
          return result;
        }).should.eventually.to.be.not.empty;
      });
    });
  });

  describe('on creating task', function () {
      it('should return true on creating task with valid input', function () {
        return underTest.createTask(req, res).then(function(createResponse){
            return deleteLatestTask().then(function(deleteResponse){
              return assert.isOk(createResponse, "should equal true");
            });
        });
      });
  });

  describe('on creating task with document', function () {
    after(function() {
      return underTest.getTasks().then(function(tasks){
        for (var index in tasks) {
          if (tasks[index].id > latestId) {
            latestId = tasks[index].id;
          }
        }
        req.params.id = latestId;
        return underTest.deleteTask(req, res).then(function(rows){
            return Promise.resolve(rows);
        });
      });
    });

    it('should return true on creating task with valid input and document', function () {
      return underTest.createTask(req, res).should.eventually.equal(true);
    });
  });

  describe('on updating task\'s detail', function () {
    after(function() {
      return underTest.getTasks().then(function(tasks){
        for (var index in tasks) {
          if (tasks[index].id > latestId) {
            latestId = tasks[index].id;
          }
        }
        req.params.id = latestId;
        return underTest.deleteTask(req, res).then(function(rows){
            return Promise.resolve(rows);
        });
      });
    });

    it('should return false on updating task with valid input', function () {
      return underTest.createTask(req, res).then(function(createResult){
        return underTest.getTasks().then(function(tasks){
          for (var index in tasks) {
            if (tasks[index].id > latestId) {
              latestId = tasks[index].id;
            }
          }
          req.params.id = latestId;
          return underTest.updateTask(req, res).then(function(updateResult){
            return Promise.resolve(updateResult);
          });
        });
      }).should.eventually.equal(false);
    });
  });

  describe('on deleting task', function () {
    it('should return 0 when deleting task when task id not available on current task table', function () {
      req.params.id = 0;
      return underTest.deleteTask(req, res).should.eventually.equal(0);
    });
    it('should return 1 when deleting task when task id available on current task table', function () {
      return underTest.createTask(req, res).then(function(createResult){
        return underTest.getTasks().then(function(tasks){
          for (var index in tasks) {
            if (tasks[index].id > latestId) {
              latestId = tasks[index].id;
            }
          }
          req.params.id = latestId;
          return underTest.deleteTask(req, res).then(function(rows){
              return Promise.resolve(rows);
          });
        });
      }).should.eventually.equal(1);
    });
  });

  describe('on reading task\'s data', function () {
    it('length should match when new data is added', function () {
      var oldLength;
      var newLength;
      underTest.getTasks().then(function(oldtasks){
        oldLength = oldtasks.length;
      });
      return underTest.createTask(req, res).then(function(createResult){
        return underTest.getTasks().then(function(tasks){
          for (var index in tasks) {
            if (tasks[index].id > latestId) {
              latestId = tasks[index].id;
            }
          }
          req.params.id = latestId;
          newLength = tasks.length;
          return underTest.deleteTask(req, res).then(function(rows){
            if (newLength == oldLength+1) {
              return Promise.resolve(true);
            } else {
              return Promise.resolve(true);
            }

          });
        });
      }).should.eventually.equal(true);
    });
  });

  describe('on assigning task to courier', function () {
    it('should not have any database\'s constraint and assigned correctly', function () {
        var req = {
          params : {
            courier_id : 3,
            task_id : 1
          },
          session : {
            fullname : "WARDOYOK HERMAN"
          }
        }

        var res = {send : function (args) {}}

        return underTest.assignTaskToCourier(req, res).then(function (result) {
          return Promise.resolve(result);
        }).should.eventually.have.length(1);
    });
  });

  describe('on upload task file', function () {
    it('should return object when reading data from valid formated uploaded file', function () {
      req.file = {"path" : "uploads/valid.json", "originalname": "test.json"};
      return underTest.readJsonFile(req, res).should.eventually.to.be.not.null;
    });
  });

  it('should return true when adding data from valid formated uploaded file', function () {
  describe('on upload task file', function () {
      req.file = {"path" : "uploads/valid.json", "originalname": "test.json"};
      return underTest.readJsonFile(req, res).then(function(Jsondata){
          return underTest.createAllTask(Jsondata, res).then(function(response){
            var tasks = Jsondata.tasks;
            deleteLatestTask();
            return assert.isOk(response,  "Should return true");
          });
      });
    });
  });

  describe('on upload task file', function () {
    it('should return true when Json format is valid', function () {
      req.file = {"path" : "uploads/valid.json", "originalname": "test.json"};
      return underTest.readJsonFile(req, res).then(function(tasks){
          return underTest.validateJsonStructure(tasks).should.eventually.equal(true);
      });
    });
  });

  describe('on upload task file', function () {
    it('should return false when Json format is not valid', function () {
      req.file = {"path" : "uploads/unStructured.json", "originalname": "testNotValid.json"};
      return underTest.readJsonFile(req, res).then(function(tasks){
          return underTest.validateJsonStructure(tasks).then().catch(function(err){
            return assert.isNotOk(err, "this will equal false");
          });
      });
    });
  });

  describe('on upload task file', function () {
    it('should return false when reading data from unsupported file format', function () {
      req.file = {"path" : "uploads/fakeType.json", "originalname": "test.json"};
      return underTest.readJsonFile(req, res).then().catch(function(err){
        return assert.isNotOk(err, "this will equal false");
      });
    });
  });

  describe('on upload task file', function () {
    it('should return true when uploading file with size below 5MB', function () {
      req.file = {"path" : "uploads/valid.json", "originalname": "test.json", "size":"3000000"};
      return underTest.readJsonFile(req, res).should.eventually.to.be.not.null;
    });
  });

  describe('on upload task file', function () {
    it('should return false when uploading file with size more than 5MB', function () {
      req.file = {"path" : "uploads/valid.json", "originalname": "test.json", "size":"6000000"};
      return underTest.readJsonFile(req, res).then().catch(function(err){
        return assert.isNotOk(err, "this will equal false");
      });
    });
  });

  describe('on upload task file', function () {
    it('should return false when uploading non json file', function () {
      req.file = {"path" : "uploads/test.pdf", "originalname": "test.pdf"};
      return underTest.readJsonFile(req, res).then().catch(function(err){
        return assert.isNotOk(err, "this will equal false");
      });;
    });
  });

  describe('on upload task file', function () {
    it('should return false when Json format is not valid', function () {
      req.file = {"path" : "uploads/unStructured2.json", "originalname": "testNotValid.json"};
      return underTest.readJsonFile(req, res).then(function(tasks){
          return underTest.validateJsonStructure(tasks).then().catch(function(err){
            return assert.isNotOk(err, "this will equal false");
          });
      });
    });
  });

});
